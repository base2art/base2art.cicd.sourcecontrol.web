namespace Base2art.CiCd.SourceControl.Web.Git.Fixtures.TestServer
{
    using Base2art.Web.App.Configuration;
    using Base2art.WebApiRunner.Server;
    using Base2art.WebApiRunner.Server.Runners;

    public class Startup : SharedStartup
    {
        public Startup(IServerConfiguration configuration, IFrameworkShim shim)
            : base(configuration, new NullApplication(), null , shim)
        {
        }
    }
}