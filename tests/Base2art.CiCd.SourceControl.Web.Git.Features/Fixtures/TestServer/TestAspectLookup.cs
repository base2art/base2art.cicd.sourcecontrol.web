namespace Base2art.CiCd.SourceControl.Web.Git.Fixtures.TestServer
{
    using Base2art.Web.App.Configuration;
    using Base2art.WebApiRunner.Server;
    using Base2art.WebApiRunner.Server.Testability.Configuration;

    public class TestAspectLookup : IAspectLookup
    {
        public ITypeInstanceConfiguration GetByName(string name) => new AspectConfiguration();
    }
}