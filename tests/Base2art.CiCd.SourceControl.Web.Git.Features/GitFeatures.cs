namespace Base2art.CiCd.SourceControl.Web.Git
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LibGit2Sharp;
    using Resources;
    using Xunit;
    using Xunit.Abstractions;

    public class GitFeatures
    {
        private readonly ITestOutputHelper testOutputHelper;

        public GitFeatures(ITestOutputHelper testOutputHelper)
        {
            this.testOutputHelper = testOutputHelper;
        }

        // [Fact]
        // public void ShouldGetBranches()
        // {
        //     using (var repo = new Repository("/home/tyoung/code/b2a/base2art.standard.messagequeue"))
        //     {
        //         foreach (var trackedBranch in repo.Branches
        //                                           .Where(x => x.Reference.TargetIdentifier != "refs/remotes/origin/master")
        //                                           .Where(x => x.IsRemote))
        //         {
        //             var name = trackedBranch.FriendlyName.Replace(trackedBranch.RemoteName + "/", "");
        //             if (repo.Branches[name] == null)
        //             {
        //                 Branch localBranch = repo.CreateBranch(name, trackedBranch.Tip);
        //                 repo.Branches.Update(localBranch, b => b.UpstreamBranch = trackedBranch.UpstreamBranchCanonicalName);
        //             }
        //
        //             // repo.Branches.Update(localBranch, b => b.UpstreamBranch = trackedBranch.UpstreamBranchCanonicalName);
        //             Commands.Checkout(repo, trackedBranch);
        //         }
        //
        //         var mappedBranches = repo.Branches
        //                                  .Where(x => !x.IsRemote)
        //                                  .Where(x => x.Reference.TargetIdentifier != "refs/remotes/origin/master")
        //                                  .Select(x =>
        //                                  {
        //                                      ListBranchesContainingCommit(x);
        //                                      return x;
        //                                  })
        //                                  .Select(x => new IsolatedBranchData
        //                                               {
        //                                                   Hash = x.Reference.TargetIdentifier,
        //                                                   Name = this.Name(x)
        //                                               })
        //                                  .ToArray();
        //         // mappedBranches
        //         foreach (var mappedBranch in mappedBranches)
        //         {
        //             this.testOutputHelper.WriteLine(mappedBranch.Name);
        //         }
        //     }
        // }

        private IEnumerable<Branch> ListBranchesContainingCommit(Repository repo, string commitSha)
        {
            var commit = repo.Lookup<Commit>(commitSha);

            IEnumerable<Reference> headsContainingTheCommit = repo.Refs.ReachableFrom(repo.Refs, new[] {commit});
            return headsContainingTheCommit.Select(branchRef => repo.Branches[branchRef.CanonicalName]).ToList();
        }

        private void ListBranchesContainingCommit(Branch repo)
        {
            // repo.Reference.
        }

        private string Name(Branch branch)
        {
            return branch.FriendlyName.Replace("refs/remotes/origin/", "");
        }
    }
}