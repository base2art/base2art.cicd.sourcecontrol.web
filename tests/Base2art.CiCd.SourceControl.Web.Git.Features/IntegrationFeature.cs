namespace Base2art.CiCd.SourceControl.Web.Git
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using Base2art.Web.App.Principals.Jwt;
    using DataStorage.Provider.SQLite;
    using Diagnostic;
    using Fixtures.TestServer;
    using FluentAssertions;
    using Newtonsoft.Json;
    using Public.Resources;
    using Resources;
    using Servers;
    using Xunit;
    using Xunit.Abstractions;

    public class IntegrationFeature
    {
        private readonly ITestOutputHelper testOutputHelper;

        public IntegrationFeature(ITestOutputHelper testOutputHelper)
        {
            RequireAuthProvider.OverridingValue = false;
            this.testOutputHelper = testOutputHelper;
            this.testOutputHelper.WriteLine(typeof(StaticKeysModule).ToString());
            this.testOutputHelper.WriteLine(typeof(ProcessBuilderFactory).ToString());
            this.testOutputHelper.WriteLine(typeof(SQLiteProvider).ToString());
        }

        [Theory]
        // [InlineData("conf.yaml")]
        [InlineData("confV2.yaml")]
        public async void ShouldGetFeatures(string confFile)
        {
            var configPath = this.Config(confFile);
            using (var fixture = IntegrationTests.CreateFixture(configPath))
            {
                var client = fixture.Client;

                var url = $"http://localhost/api/v1/supported-features";
                var response = await client.GetAsync(url);

                response.StatusCode.Should().Be(HttpStatusCode.OK);
                response.Content.Headers.ContentType.MediaType.Should().Be("application/json");

                var responseString = await response.Content.ReadAsStringAsync();

                var features = JsonConvert.DeserializeObject<List<SupportedFeature>>(responseString);
                features.Count.Should().Be(1);
                features[0].Id.Should().Be(new Guid("db7ab9e6-8258-4fce-91d0-851cdccff8cc"));
                features[0].FullName.Should().Be("Base2art.CiCd.SourceControl.Web.Git.Public.Resources.GitRepositoryData");
                features[0].Name.Should().Be("GitRepositoryData");
                var defaultData = features[0].DefaultData;
                var location = ConvertTo<GitRepositoryData>(defaultData);
                string cloneLocation = location.CloneLocation;
                cloneLocation.Should().Be("git@gitlab.com:base2art/cicd/sample-project.git");
            }
        }

#if DEBUG
        [Theory()]
#else
        [Theory(Skip = "Skipping env in release")]
#endif
        // [InlineData("conf.yaml")]
        [InlineData("confV2.yaml")]
        public async void ShoulGetSourceBranches(string confFile)
        {
            var configPath = this.Config(confFile);

            using (var fixture = IntegrationTests.CreateFixture(configPath))
            {
                var client = fixture.Client;

                var taskUrl = $"http://localhost/tasks/provision-db";
                using (var stringContent = new StringContent("{}", Encoding.UTF8, "application/json"))
                {
                    var response = await client.PostAsync(taskUrl, stringContent);
                    var responseString = await response.Content.ReadAsStringAsync();
                    testOutputHelper.WriteLine(responseString);
                }

                var url = $"http://localhost/api/v1/source/branches";
                var gitRepositoryData = new GitRepositoryData
                                        {
                                            CloneLocation = this.CloneLocation
                                        };
                using (var stringContent = new StringContent(ToJson(gitRepositoryData), Encoding.UTF8, "application/json"))
                {
                    var response = await client.PutAsync(url, stringContent);
                    var responseString = await response.Content.ReadAsStringAsync();
                    testOutputHelper.WriteLine(responseString);

                    response.StatusCode.Should().Be(HttpStatusCode.OK);
                    response.Content.Headers.ContentType.MediaType.Should().Be("application/json");

                    var features = JsonConvert.DeserializeObject<List<IsolatedBranchData>>(responseString);
                    features.Count.Should().Be(3);

                    features[0].Name.Should().Be("feature/test-branch");
                    features[0].Hash.Should().Be("a2e771cb4de18e736bfbdf465c82e8af2731fb6c");

                    features[1].Name.Should().Be("feature/test-branch-2");
                    features[1].Hash.Should().Be("2d931a4b9a62f23b6719fa83bd9f6af6bc1f3c1d");

                    features[2].Name.Should().Be("master");
                    features[2].Hash.Should().Be("a2e771cb4de18e736bfbdf465c82e8af2731fb6c");
                }
            }
        }

#if DEBUG
        [Theory()]
#else
        [Theory(Skip = "Skipping env in release")]
#endif
        // [InlineData("conf.yaml", true)]
        [InlineData("confV2.yaml", false)]
        public async void ShoulGetSourceLogs(string confFile, bool legacy)
        {
            var configPath = this.Config(confFile);
            using (var fixture = IntegrationTests.CreateFixture(configPath))
            {
                var client = fixture.Client;

                var taskUrl = $"http://localhost/tasks/provision-db";
                using (var stringContent = new StringContent("{}", Encoding.UTF8, "application/json"))
                {
                    var response = await client.PostAsync(taskUrl, stringContent);
                    var responseString = await response.Content.ReadAsStringAsync();
                    testOutputHelper.WriteLine(responseString);
                }

                var url = $"http://localhost/api/v1/source/logs";
                var gitRepositoryData = new GitRepositoryBranchData
                                        {
                                            Repository = new GitRepositoryData
                                                         {
                                                             CloneLocation = this.CloneLocation
                                                         },
                                            Branch = new IsolatedBranchData
                                                     {
                                                         Hash = "2d931a4b9a62f23b6719fa83bd9f6af6bc1f3c1d",
                                                         Name = "feature/test-branch-2"
                                                     }
                                        };
                using (var stringContent = new StringContent(ToJson(gitRepositoryData), Encoding.UTF8, "application/json"))
                {
                    var response = await client.PutAsync(url, stringContent);
                    var responseString = await response.Content.ReadAsStringAsync();
                    testOutputHelper.WriteLine(responseString);

                    response.StatusCode.Should().Be(HttpStatusCode.OK);
                    response.Content.Headers.ContentType.MediaType.Should().Be("application/json");

                    var features = JsonConvert.DeserializeObject<IsolatedBranchLogData>(responseString);
                    features.Name.Should().Be("feature/test-branch-2");
                    features.Hash.Should().Be("2d931a4b9a62f23b6719fa83bd9f6af6bc1f3c1d");
                    features.Logs.Length.Should().Be(5);
                    features.Logs[0].Id.Should().Be("2d931a4b9a62f23b6719fa83bd9f6af6bc1f3c1d");
                    features.Logs[0].When.Should().Be(DateTime.Parse("2020-07-29T23:08:48"));
                    features.Logs[0].Author.Should().StartWith("Scott ");
                    features.Logs[0].Message.Should().StartWith("Update 00-clean.ps1");
                    features.Logs[0].FilesAdded.Should().BeEmpty();
                    features.Logs[0].FilesRemoved.Should().BeEmpty();
                    features.Logs[0].FilesChanged.Should().HaveCount(1);

                    features.Logs[2].Id.Should().Be("220b97db83207dbe4aedefe7c61971e8369bab3c");
                    if (legacy)
                    {
                        features.Logs[2].When.Should().Be(DateTime.Parse("2018-12-13T12:43:40"));
                    }
                    else
                    {
                        features.Logs[2].When.Should().Be(DateTime.Parse("2018-12-13T20:43:40"));
                    }

                    features.Logs[2].Author.Should().StartWith("Leat ");
                    features.Logs[2].Message.Should().StartWith("Adding in provision script");
                    features.Logs[2].FilesAdded.Should().HaveCount(2);
                    features.Logs[2].FilesRemoved.Should().BeEmpty();
                    features.Logs[2].FilesChanged.Should().HaveCount(1);
                }
            }
        }

        #if DEBUG
        [Theory()]
#else
        [Theory(Skip = "Skipping env in release")]
#endif
        // [InlineData("conf.yaml", true)]
        [InlineData("confV2.yaml", false)]
        public async void ShouldGetSource(string confFile, bool legacy)
        {
            var configPath = this.Config(confFile);
            using (var fixture = IntegrationTests.CreateFixture(configPath))
            {
                var client = fixture.Client;

                var taskUrl = $"http://localhost/tasks/provision-db";
                using (var stringContent = new StringContent("{}", Encoding.UTF8, "application/json"))
                {
                    var response = await client.PostAsync(taskUrl, stringContent);
                    var responseString = await response.Content.ReadAsStringAsync();
                    testOutputHelper.WriteLine(responseString);
                }

                var url = $"http://localhost/api/v1/checkout";
                var gitRepositoryData = new GitCloneData
                                        {
                                            Repository = new GitRepositoryData
                                                         {
                                                             CloneLocation = this.CloneLocation
                                                         },
                                            BranchOrTag = "feature/test-branch-2"
                                        };

                Guid id;
                IsolatedCheckoutState state = IsolatedCheckoutState.Unknown;

                using (var stringContent = new StringContent(ToJson(gitRepositoryData), Encoding.UTF8, "application/json"))
                {
                    var response = await client.PutAsync(url, stringContent);
                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK);
                    response.Content.Headers.ContentType.MediaType.Should().Be("application/json");
                    var checkoutData = JsonConvert.DeserializeObject<IsolatedCheckout>(responseString);
                    id = checkoutData.Id;
                    state = checkoutData.State;
                    state.Should().Be(IsolatedCheckoutState.Pending);
                    id.Should().NotBeEmpty();
                }

                while (!(state == IsolatedCheckoutState.CompletedFail || state == IsolatedCheckoutState.CompletedSuccess))
                {
                    var getStateUrl = $"http://localhost/api/v1/checkout/" + id.ToString("N");

                    var response = await client.GetAsync(getStateUrl);
                    var responseString = await response.Content.ReadAsStringAsync();
                    testOutputHelper.WriteLine(responseString);
                    response.StatusCode.Should().Be(HttpStatusCode.OK);
                    response.Content.Headers.ContentType.MediaType.Should().Be("application/json");
                    var checkoutData = JsonConvert.DeserializeObject<IsolatedCheckout>(responseString);
                    id = checkoutData.Id;
                    state = checkoutData.State;

//                    state.Should().Be(IsolatedCheckoutState.Working);
//                    id.Should().NotBeEmpty();

                    await Task.Delay(TimeSpan.FromSeconds(4));
                }

                state.Should().Be(IsolatedCheckoutState.CompletedSuccess);

                var sourceUrl = $"http://localhost/api/v1/checkout/{id:D}/source";
                var responseSource = await client.GetAsync(sourceUrl);
                var responseSourceString = await responseSource.Content.ReadAsStringAsync();
                testOutputHelper.WriteLine(responseSourceString);
                responseSource.StatusCode.Should().Be(HttpStatusCode.OK);
                responseSource.Content.Headers.ContentType.MediaType.Should().Be("application/json");
                var checkoutDataBytes = JsonConvert.DeserializeObject<byte[]>(responseSourceString);

                checkoutDataBytes.Length.Should().Be(6003);

                var deleteStateUrl = $"http://localhost/api/v1/checkout/{id:D}";

                var message = new HttpRequestMessage(HttpMethod.Delete, deleteStateUrl);
                
                using (var stringContent = new StringContent("", Encoding.UTF8, "application/json"))
                {
                    message.Content = stringContent;
                    var deleteResponseSource = await client.SendAsync(message);
                    var deleteResponseSourceString = await deleteResponseSource.Content.ReadAsStringAsync();
                    deleteResponseSource.StatusCode.Should().Be(HttpStatusCode.OK);
                    deleteResponseSourceString.Should().BeNullOrWhiteSpace();
                }

//                var sourceUrl = $"http://localhost/api/v1/checkout/{id:N}/source";
                responseSource = await client.GetAsync(sourceUrl);
                responseSourceString = await responseSource.Content.ReadAsStringAsync();
//                testOutputHelper.WriteLine(responseSourceString);
                if (legacy)
                {
                    responseSource.StatusCode.Should().Be(HttpStatusCode.OK);
                    responseSource.Content.Headers.ContentType.MediaType.Should().Be("application/json");
                    checkoutDataBytes = JsonConvert.DeserializeObject<byte[]>(responseSourceString);

                    checkoutDataBytes.Length.Should().Be(22);
//                    responseSourceString.Length.Should().Be(22)
                }
                else
                {
                    responseSource.StatusCode.Should().Be(HttpStatusCode.NotFound);
                    responseSource.Content.Headers.ContentType.MediaType.Should().Be("application/json");
//                checkoutDataBytes = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseSourceString);
                }

//                testOutputHelper.WriteLine(responseSourceString);
//                checkoutDataBytes.Length.Should().Be(0);
            }
        }

        public string CloneLocation
        {
            get
            {
                var path = Path.Combine(
                                        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                        "base2art",
                                        "cicd",
                                        "git-repo.url");

                return File.ReadAllText(path).Trim();
            }
        }

        private string Config(string confFile)
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            path =  Path.GetDirectoryName(path);
            path =  Path.GetDirectoryName(path);
            path =  Path.GetDirectoryName(path);
            path =  Path.GetDirectoryName(path);
            path =  Path.GetDirectoryName(path);
            path =  Path.GetDirectoryName(path);
            path = Path.Combine(path, "config", confFile);

            return path;
            
            // return "/home/tyoung/code/b2a/base2art.cicd.sourcecontrol.web.git/config/" + confFile;
        }

        private string ToJson(object gitRepositoryData)
        {
            return JsonConvert.SerializeObject(gitRepositoryData);
        }

        private T ConvertTo<T>(object value)
        {
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(value));
        }
    }
}