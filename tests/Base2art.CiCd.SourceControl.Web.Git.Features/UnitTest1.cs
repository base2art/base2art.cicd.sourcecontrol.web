namespace Base2art.CiCd.SourceControl.Web.Git
{
    using System;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Runtime.Remoting.Proxies.Internals;
    using System.Security.Claims;
    using Base2art.DataStorage;
    using Base2art.DataStorage.InMemory;
    using Base2art.Diagnostic;
    using Data;
    using FluentAssertions;
    using Public.Endpoints;
    using Public.Resources;
    using Resources;
    using Xunit;
    using Xunit.Abstractions;

    public class UnitTest1
    {
        private readonly ITestOutputHelper helper;

        public UnitTest1(ITestOutputHelper helper)
        {
            this.helper = helper;
        }

        [Fact]
        public void JWT_Creation()
        {
//            new Base2art.Web.App.Principals.Jwt.AsymmetricModule();
        }

        private static void NewMethod(DirectoryInfo basePath, DirectoryInfo output, byte[] contentPairValue)
        {
            File.WriteAllBytes(basePath.FullName + ".zip", contentPairValue);

            ExtractZip(output.FullName, contentPairValue);

            Assert.True(File.Exists(Path.Combine(output.FullName, "Tests", "test.cs")));
        }

        private static void ExtractZip(string fullName, byte[] contentPairValue)
        {
            using (var ms = new MemoryStream())
            {
                ms.Write(contentPairValue, 0, contentPairValue.Length);

                ms.Seek(0L, SeekOrigin.Begin);
                using (var source = new ZipArchive(ms))
                {
                    foreach (var entry in source.Entries)
                    {
                        var fullPath = Path.GetFullPath(Path.Combine(fullName, entry.FullName));
                        if (!fullPath.StartsWith(fullName, StringComparison.OrdinalIgnoreCase))
                        {
                            throw new IOException("IO_ExtractingResultsInOutside");
                        }

                        if (Path.GetFileName(fullPath).Length == 0)
                        {
                            if (entry.Length != 0L)
                            {
                                throw new IOException("IO_DirectoryNameWithData");
                            }

                            Directory.CreateDirectory(fullPath);
                        }
                        else
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
                            entry.ExtractToFile(fullPath, false);
                        }
                    }
                }
            }
        }

        [Fact]
        public async void Test1()
        {
            var basePath = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N")));

            var testDir = Directory.CreateDirectory(Path.Combine(basePath.FullName, "Tests"));
            File.WriteAllText(Path.Combine(basePath.FullName, "hello.txt"), "HI!");
            File.WriteAllText(Path.Combine(testDir.FullName, "test.cs"), "public static void main(){}");
            var contentPairValue = await basePath.ZipToBytes();

            var basePathOutput = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N") + ".output"));
            NewMethod(basePath, basePathOutput, contentPairValue);

            basePathOutput = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N") + ".output"));
            NewMethod(basePath, basePathOutput, Convert.FromBase64String(Convert.ToBase64String(contentPairValue)));
//            var contentPairValue = Convert.FromBase64String(Convert.ToBase64String(contentPairValue));
        }

        [Fact]
        public void Test2()
        {
            var value =
                "UEsDBBQAAAAIAKWNfE3f/D9EfAAAAJoAAAAKAAAALmdpdGlnbm9yZUXMQQrCMBCF4f3AXMTFiEeQuLcguJVJHNrYmBmapLS3t+3GzYMHH7/6DwKCjxnhUdckTo0ch0H+/xk1cY2aCy3fhHA3yU5nmRCMw8i9FARt1VpFOJHncU+WMLEhUG691POxJMuWvXbd68aVd1vyZuktlnRFOJC1MpCVyw9QSwMEFAAAAAgApY18TadmRPGNAgAAnQcAAAcAAABib2IucHMxlVVdb9owFH1H4j9cuXlIKtlsr5PQVLXQVlo/1GbTXoPjgktiU9tBq7b+9/kjIQECbW0JEd97zz33+thmdCEB0YJlgos5FJJmBawyuszmTBNC0HAwHLD3naKVks8axnDJDD5f8CK/NqwEfJ+ZBSCtqPV5kopldAFxxJ2NCwhRyXDwdzgAO6IZFz5iDCiKwfuRaVUUt1nJIBlZ8wgFV/4Eccq0CRmawCQYazg3Hlgp1wwHNg+MVkozwFOpKNtEBe+3moOcPR/jYM0HONSBn+RQR204vO13PLQ4l0YwA34RMKZSPPF5pTLDpbAp7LJDXcNLCzCr7EbsAvjFdwA+4RpmyGdsMzbpjkvCueqPiYLqY0g7G4SnvDBMATolIc4r2OG0mYLB56qx9/fsxFUH7XfdEUf7YEO8XgJiRzEbAo1ksJAm+l6n3EnrBvvDDUQ/zh7Tye/r9PzuYtLa37Z14mY0U5mgC5/MajZna+xPqM27LVBEgicW1hUlTdrteN9iKQwTts6eMPgHd5XBj0bZje4D6HyRVPEyTjZcozVT2jVsDPHlJMUXZ+kkIan8KbizZEXKSxa7lQAfo9dXcnOT5+TqqiyJ1shinXRQ0Bfi5teeUmunTpltWF+NjX9ffW1k83e3Mjcdg24rMHsBVGbaqrFlEX7DaXE36Oa0dDTm1q3GuKBFlTOsX8uZLHR3RVbu6vAH8LAYR6tv9+GO/hVIj6O44e9E2dLxoryVpttAWZmV7UNzy6Nk/4iUy5yrfc+t23Sr3kov3PthZHhC2sLrPn/gDQE7jlwZztxh6LfE3w9hR4RtC5Vl6XRx8PjR1eHHp27u6JSIarWcIyCjnfJH22Cq7HE5JfWeBpS+0+2V9R9QSwMEFAAAAAgApY18TVuybJbXAgAAcwkAAA8AAABEd3QuSGFiaXRhdC5zbG6tVktu2zAQXceA72C4mxQIBf5kiosuRFFsu2gR1GhX3cgy7aqVJUGiUgRuTtZFj9QrlJItx78kjpuFIXLmzZvHmSHhv7//9HsfkrjMq3xmBl+Sqo7SwdjU0yQfjPO0NkmeDVSS6quBystFZDG6rBojwg6E/d6rvSAMEW6s429RWUh9o9O8GLgO6vdWuBWsI3nT0jgYepg5pJGSJYt68QAUWiiFGPGG7rrMv+vYXA6XGLkwJIQAT8kAUOwTwCmjAPmEuyNJRzL07oavLcPQ6MpUw6utxdKjrvSVhwAfKWWjoQAcCwh8GDJMKWZChnfDfi/MpuuU27mVH0IaBhAQiBRASBIgFBUAwgBSxXioRLDOLX8a5100SUxkHKUjU5f6XsrXY96jRieuCpu+1U5YqKx4CTji9uRccMADxIAIxMhHXsAhpC+tvUlclfG2th2dW/KkcilhXAGFOQNUQgoEg9hqDEfcrqR02cPyntHWOM9mTcbuu+Scj6TnMSDDpjAM2VgXS2DPSJTAAWSB32S+WKcb258dsstu5N8bvaga6qLUG2kXFw3/zmGzuip0bHEPeJogCEGc6ihzigpZ5Pa2dSOQ1XNtbGtNXuoOdWhtwRjMdabLyOhxXpexHYc1/qijDSFgUU3qJJ120B1DC6GrZEUU/4jmGwmH1gbcRoKpLtL8do3cN1nYfUvXpX2BLk/zuL0w3XfJiC9IECjgjYSNRZICDzLPXl2ChSuJwAKd0eVSR9OFdhZT69msnz7T2zSfRKnFrRb7yQI7Icm8th2ym+s0MjP7nnbZO1CTXupJPf/lZ7eD4Pqzde/sG8Anbeen0luQPctK646MA1lr2Y+oyiuzLeu0x8bZUev4NtmNDmbzY+c4i1G0gwv/g2+vWDsaDwt5Luu9zmOcJ72Mz6rlOYyP1fI0vufWUp7F+lgtnxz0j/YF1d0tPXbfTmuwjTvtr8JRTRtLv/cPUEsDBBQAAAAIAKWNfE3LyFY1GQAAABcAAAAJAAAAcmVhZG1lLm1ke797Py+XskJwfmlRcqqCc35eSVF+Di8XAFBLAwQUAAAACACljXxNtp0ZtY8AAAAPAQAAGQAAAHNyYy9jb21tb24vQ29tcGFueUluZm8uY3MrLc7MS1cIriwuSc3VC0pNy0lNLsnMz7Pm5eLlik4sLk7NTcqptIIpcPYJds7PLcjJTMwr0SgpKk3VjEVR5ghlgRQl5lVqKLkklmUWK4QXZaZnlCiEFKXmJmbmpSr4+AQo4dRZUAlWraEEZyocWqmA0yAFIwNDU1ymhRQlpgCVFmVrKOG0sDSnpLQoFaoAAFBLAwQUAAAACACljXxNu2plq4cAAADiAAAAGQAAAHNyYy9jb21tb24vUHJvZHVjdEluZm8uY3MrLc7MS1cIriwuSc3VC0pNy0lNLsnMz7Pm5SpFkSnNK8nMTdXzzCtJLcovCE4tKstMTi0GKuPlik4sLk7NTcqptFJwhLICivJTSpNLNJRcykv0PBKTMksSS5Q0Y7GqDUstKgbaqKFkqGcAgrjUuWXmpBJS65yfG5ZZnJmUk6qRlphTnAqSBgBQSwMEFAAAAAgApY18TUDvtYThAAAAlgEAACIAAABzcmMvRHd0LkhhYml0YXQvRHd0LkhhYml0YXQuY3Nwcm9qjVHNTsMwDL5X6juE3OuuLRKXNDd+htg0wcQ9SrxqdI0nN1PFs3HgkXgF2lGg6mk+2Z/tz5/tr49PtWF6QxvEi6tLudpbppZ2Ada3W+ghqeMojkRvQ+ERObzfM52O+gcc7N9TW8MVhjs2DXbEtfYY2mC8M+xyWKh0np+0PhlfvSK3e/L6BgqVToFRQTqXMOLLgM1clbpKErUxtjYVPuMOGb1FsfT2cHJYyofQHPKV4dpR56UYB5WygBwyKK4zKVKdJJcTrrEL5M+ne2xpSpllsID8j+93l6noODrvNvyhj74BUEsDBBQAAAAIAKWNfE3K75D8bAEAAE0DAAAbAAAAc3JjL0R3dC5IYWJpdGF0L0hhYml0YXRzLmNzhVJLTsMwEN1X6h2srhIJcgD6kVBaoKK0SBELRLtwk6FYJONo7LSKUE/GgiNxBZzEpUnKZzaOnifvM57P9w/kCaiUh8DGO+3d8LXQXHc7b90OM5UpgRsW5EpD0j+FPF/GMYRaSFTeNSCQCBttU9RAyGNl0ApPs3UsQqaMijnCmCvFrKqqOqx02UxiyzUcugl4JDHO2ViUmpzygdJkhM7YVYbhYFrZGo3YHQ9fBMJMytcsZcMjZVEIu/8pnKCEfZmknIC8BUXCJJluUBL4XIH7nakoa/EQxXGPV7VARTWsPfWCyeXs/HHxMA96KzZkjsuGo9JhoDlGnKLKkNPzL5bLXiF6INrX9ZtztSnsM/1qZQO6CbTui9pyYkll2dib4FaQxARQezbI3GxQ//Q/8cycRlSzLKi5QHULuWMZ3dqY/vBQFIHOCFvTszQrpz6XH+fToilXIDfLL0I73xPknuRWRECO2yY3xK1Pc+y/AFBLAwQUAAAACACljXxNRPJMu4oAAAAfAQAAGgAAAHNyYy9Ed3QuSGFiaXRhdC9JU3lzdGVtLmNze797f15ibmpxQWJyqoJLeYmeR2JSZkliCS9XNS+XAhCUFmfmpSsEVxaXpObqefpb83JBxAtKk3IykxUy80pSi9JAmj0haiCyUM0g4JJZlJpckl9U6ZmXlq8QlJ9folCtkJ5aYq1QCzMLU5lLYkkikjLsinzy04sJKnItKsovIqzMsaAAXRGQqgUAUEsDBBQAAAAIAKWNfE33HS24OwAAAEIAAAAfAAAAc3JjL0R3dC5IYWJpdGF0L3BhY2thZ2VzLmNvbmZpZ3u/e7+NfUVujkJZalFxZn6erZKhnoGSQmpecn5KZl66rVJpSZquhZK9HS+XTUFicnZiemoxiK0P5wAAUEsDBBQAAAAIAKWNfE0yUIiZaAEAAJgEAAAaAAAAc3JjL0R3dC5IYWJpdGF0L1N5c3RlbXMuY3O9U9tqAjEQfS/4D4NPu7DkB9oVxFW6ULRgwee4jW7obhJy8ULxy/rQT+ovNDHZra1KpRTnZcgwc86ZSz7e3hmuiRK4IJCtNbrHc6qx7ty8dm7AmlGULWG6VZrUt8chlE9s1MeFmVe0AGXLrSsqrFTIUj4hQB7nZlSSQnO5zdmCQ1+IDGsc6ZIqyD0CqL1LbIV07FiIsdUdfyEegDuTRBvJHPJkETXFoSqBDaQ92CBHE4eunO2aVi6Q+MCX6goSHc1fJQ6l5PIaIj3ReZmSrrAmp3V65As0JjAyrLgLScl3mB5UnL8Ycb4RuoCgH9IUmKmqg9wT+c50KfkaGFlDXy5NTZge27rhpiBCU84i93V4M5b4sP+jGbQS9j2hXDmkiZyVVJOp+31Rs4r4v2S1gL/pWmEJz/Yr2IlCCo9Yl2jA6zllJPJTbTpEI8vjd9HNZk/d9hIcxbm78UtCA0nsDbTvKBD+OJrgdp9QSwMEFAAAAAgApY18TVdNleakAAAAGwEAAC0AAABzcmMvRHd0LkhhYml0YXQvSW50ZXJuYWxzL0R5bmFtaWNEcml2ZUluZm8uY3OFTkkOwjAMvFfqH/yCPoBeeyAnpMIH0mAqS2lSxQYUVX0ZB57EF0gXKsQFH7yMZsbzejyd7pB7bRCquxR73ZBoKZQTDE5bzrMhzyDVlcm1cIws2BXqUObZgtPKBGM1M1QxGZKpAt1QuYuHHajtWBSr4VT9tbFkoPHeguIa9TnCAC1KCTy18fPlizy7nWKfAm/bXw0FNOJDnCPV3suG/GonVRrjG1BLAwQUAAAACACljXxNVsbSOpsBAAA8BAAAKgAAAHNyYy9Ed3QuSGFiaXRhdC9JbnRlcm5hbHMvRHluYW1pY1N5c3RlbS5jc41TwU7DMAy9I+0fcmwv+QDGOKAyqITYBIidQ+uxSFlSnLRbQfsyDnwSv4DTdlszxoQvbe1n+z3b/f780mIJthAZsGTl+K14kU44nmoHqIWyg7OPwRkjK63Ur+yxtg6Ww98unk6Oee+kfiN/G5FdUZYpYS1LauotsxbJzruUK2GhhXeNvRUoK+GAIYjcaFWzO/FeXyQSIXMG61TPzSWVn5vhiZw0IQ9M0VQyB6R4+7Kj1ySVL0pmIbXoj8R4n9aj6s0tpOVbGBsdb7UDet4E0rA6JitqMDfgHoxx3hP3NG6OcDcVIFI3FtRhPp2NLvct+bNQJYTqu5GFmb3e0d+aK4EsJxmBds/7MO/Q+GwBCNHak1vz1D7Qvup/45vVPNUFsNGI7T74WK4hP1VkgkTvqk7AZqBzuthtQS91p5+PS6Xu6Q85VWos0boJJjAXpXKR30+IlnMW0WhoxVTtoNLBFL0huBJ1cw/BIqJrXUk0egna+cGOjSINU+EWQeSxgEwK1Ub5TOrcrGwcN8J6p+MtOJ9e6zycQnhw3WPzA1BLAwQUAAAACACljXxN5M9mOgYBAADsAgAAMgAAAHNyYy9Ed3QuSGFiaXRhdC9JbnRlcm5hbHMvRHluYW1pY1N5c3RlbVByb3ZpZGVyLmNzlVLBasMwDL0X+g86plD8AdtaGAQ2w6Cj7W3s4LraMKR2KjttzOiX7bBP2i/MidMsLWVd3sWSrPckS/7+/NJigzYXEiHdO/YoVsoJx7h2SFpkdjj4GA4goLBKv8PCW4cbxme3F6JPSm9DPN6oRgFkJqyF1IdCSsbMZzI7tUaCG+ApqR0eA5HaVKyQF6tMySaL6zfz8goP6CorGf2mdRgVCF1BGloSC5TasV3SJbAFZihdUsJkChr3x75bqb/pJzhr6irmxrhUUShvyMMESnYSGfeU43aOYh2FeLT7StSvXvoca5HW66Fy+N+8787HPO6sfBq3UV6TWpp7IuGTUfM36/rRDMfhB1BLAwQUAAAACACljXxNaUPCDZMAAADoAAAAJwAAAHNyYy9Ed3QuSGFiaXRhdC9JbnRlcm5hbHMvSURyaXZlSW5mby5jc3u/e39eYm5qcUFicqqCS3mJnkdiUmZJYomeZ15JalFeYk4xL1c1L5cCEJQWZ+alKwRXFpek5up5+lvzckHEM6EqIYw0kEGeLkWZZameeWn5ECVQE0AgKT8/R8GzOCg1MaVSoVohPbXEWqEWZhQIgLWGVBYAnQNnYVWXWZSaXJJfVAmyRiEoP78ELoKkHqQSSNUCAFBLAwQUAAAACACljXxNuJh+uV0AAAB+AAAAKwAAAHNyYy9Ed3QuSGFiaXRhdC9JbnRlcm5hbHMvSURyaXZlUHJvdmlkZXIuY3N7v3t/XmJuanFBYnKqgkt5iZ5HYlJmSWKJnmdeSWpRXmJOMS9XNS+XAhBkQkUgjDSQBk+Xosyy1ICi/LLMlNQiiDKoahCASHvmpeVHxyq4p5aAWBqa1hAFtbxctQBQSwMEFAAAAAgApY18TRz6cSq/AAAATgEAACsAAABzcmMvRHd0LkhhYml0YXQvSW50ZXJuYWxzL1N0YW5kYXJkU3lzdGVtLmNzfY+xDcJADEX7SNnBJTQ3AAgKoCAVEkxgEgOWwl1kG1CEmIyCkViBC4kioMCdv/5/337eHx6PpBXmBIuLuSVu2dBc5o3EY6lpck0TiHNS9nvY1Gp0dNlqnCatzp0T8hJVYWPoC5SiNcKoS8xQqfV3uGYq4TMagRAWwZc1qElTIiHYgqWveHtP25LzH/zgOzCEyRTswOo6ASb/WOFMIlzEx1kotyB15ncB1jHRgHrVzeOBRv0++KwYRnLDvKXJ7QVQSwMEFAAAAAgApY18Tb1Cp4TkAAAAbQIAACcAAABzcmMvRHd0LkhhYml0YXQvSW50ZXJuYWxzL1N5c3RlbUJhc2UuY3OlkU1qwzAQRvcG32HIKoGiA9S00MQtNZS2tCcYK1NHIEtGM6aEkJN1kSPlCpVr52/VgGc3w/e9B9L+Z+ewJm5QE+Tfop6xNIKiCicUHFpOk02aQJyWjavgc81CtSresjTp72ZIApYsAbWAtsg8JOfIBLdQ9FvfGIDdNG1pjT5VcxNIiw/rwn15+PBeYAMVSQbbg++sdpnOURDu7k9XtQiEQsd9+o6yUgtfl8bRVFaGVWdQT621r/EVbmCyjIzJbJb9K3vxFY+V2ci4SvYYgg+jdfRHuUr40DSjdRgZvazTxO/b/gJQSwMEFAAAAAgApY18TSQNfqNnAAAAZQAAACoAAABzcmMvRHd0LkhhYml0YXQvUHJvcGVydGllcy9Bc3NlbWJseUluZm8uY3N7v3s/L1dpcWZeukJwZXFJaq5eUGleSWZuqp5zfm5BZk5qUXBqUVlmcmqxNS8XL1d0YnFxam5STqWVgmdeSWpRXmJOcVhmcWZSTmpIvoaSS3mJnkdiUmZJYomeW2piSWlRarGSZiwAUEsDBBQAAAAIAKWNfE22nRm1jwAAAA8BAAAbAAAAdGVzdHMvY29tbW9uL0NvbXBhbnlJbmZvLmNzKy3OzEtXCK4sLknN1QtKTctJTS7JzM+z5uXi5YpOLC5OzU3KqbSCKXD2CXbOzy3IyUzMK9EoKSpN1YxFUeYIZYEUJeZVaii5JJZlFiuEF2WmZ5QohBSl5iZm5qUq+PgEKOHUWVAJVq2hBGcqHFqpgNMgBSMDQ1NcpoUUJaYAlRZlayjhtLA0p6S0KBWqAABQSwMEFAAAAAgApY18Tb7g7NqAAAAA2wAAABsAAAB0ZXN0cy9jb21tb24vUHJvZHVjdEluZm8uY3MrLc7MS1cIriwuSc3VC0pNy0lNLsnMz7Pm5SpFkSnNK8nMTdXzzCtJLcovCE4tKstMTi0GKuPlik4sLk7NTcqptFJwhLICivJTSpNLNJQcCzKLlTRjsSoKSy0qBlqloWSoZwCCuNS5ZeakElLrnJ8bllmcmZSTqpGWmFOcCpIGAFBLAwQUAAAACACljXxNYVE66ysBAADQAgAANgAAAHRlc3RzL0R3dC5IYWJpdGF0LkZlYXR1cmVzL0R3dC5IYWJpdGF0LkZlYXR1cmVzLmNzcHJvapWSz07CQBDG7yS8Q9O7U1vlYAJNjIhykBBtPHFZtgNZKbvN7Kzos3nwkXwFlwKx5U+Cmz1MZvb7fZOZ/fn67o7JvKHk4CVf9MInJclYM2MY3WfgU2HabrVbgT/rhyUSfz6QcWW6SVaFTNAceUBiiStDi1QjS0MoyjKBuBvtl3fASju0YyEXYlpgOhOFxW5Uy2yNo33nbX7IuDxophLP8RlnSKglBkMtC5djLxwUDjXfWutRymgbBq9I1ke9sAPXkIRBdB6oOaQMLVeT+sPFHbiBy7N5H04rrskT380/1UBOayR4V9b5MbLLlWkQryDeI/YNj5DvCpUZUxzh5ob9Ii8Om2ug/L5qWzi5mN0vO+IDMPHXkpz0VwyPYqpYcD0GaUuvPu1ZfZA1Pf0FUEsDBBQAAAAIAKWNfE3iZhqEpAEAAOkEAAAvAAAAdGVzdHMvRHd0LkhhYml0YXQuRmVhdHVyZXMvRW52aXJvbm1lbnRUZXN0ZXIuY3OVU8tOwzAQvCP1H6ycHKlyEUcQh0JaKAKESMWbwzZZFYNrR7YTWiG+jAOfxC/gPAop0LTsxcqud3ZmNv54e5cwQZNAhCR4tuwQRtyCZX0Em2o0rY2X1gZxkRoux6QvUpS2awxqy5U0O/XiQFrUEsRi9iqV3LpMmUvSkeARiQQYQ3oy41rJiYMconHN5Z1qYh63wwdUenZfywyk4BIDsEA9x/gSR5tbXpt4wfbdnefXblajMsVjEj6oVMTHCmJqrM5p5bLbpPrAaYKRxfgCnD7/G6LGJI8MNEm0yniMmuwSic/kyCgZzhz3yVlVoKEFGYOO+3xaWMjO0ahUR3gCEsao2QHasJhLcxK+X9k1j06HLCYG5QBiyqMcHMxcM4/KEp2z+olVtrBzpdxKUyFO3UBWmkF9tod0UXmt+3W+scL0PkSWhk88cdO98PF6DaMDzOhyK9fQ9Cvz5bD/b5neTfF3NOpbR9FWg6Tq6RgWrmSzP2fTjNBNEtOAAK68oOkvjPyhsOUYsSuvxOhprXQTEywurMQ5VuMmFOHKKzHWcbW24+p4/QRQSwMEFAAAAAgApY18TU8jg2a8AQAAXwQAADAAAAB0ZXN0cy9Ed3QuSGFiaXRhdC5GZWF0dXJlcy9Kc29uU3lzdGVtUHJvdmlkZXIuY3OFU8FO3DAQvSPxD6M9JRKyUI+lICECNAgBAm6rPTjZWTDy2lt7stm02i/roZ/UX8DeOKkjsmUujsfvzbw3dv7+/qP4Eu2KlwhZTew7LwRxYlfIqTJoDw9+HR6Ai8oK9QJPjSVcnnxMsfx+LHsr1I9BPleERnFpB9k7rEkrqxfEbqxW/zvrSrYQEcpBKbm14AFt5wej12KOBr5Cnhmxxi7R8oIpHyt3ygnBIJ9rJRuwZHzft1ZIBKwKKcqRHknESOH0DOhVWOZ3cLq3TKsqVws9ncE1kv9K0n+wSKGPNTcwr6nG4viLq+pFXGi1RkMsQ4tGcCl+4n3xhiV9u2nX6ews6aWkAxE+DLobVn1V9oTSkZJN7+DCjYQw2aQpe9bnxvAm8VU6/nbgKkwxa9x7EmVvDkKRIAk2n1j0vOdmhc5jknWbFC5VtWQP3FhMyCX0Ijo8gnABKWymkz4/mcViuwbCPrp7bnz5Qmu5o+RtbjIbwS8qKe/cH+IJUZdHrSkTxjnSxhGnk6uA2xUZnbPC+sN0hsDxsUTRe3Nq+kkd7YUHXw4cXO+HDvw4wk5st989zW4Q0f1FsR2+i7Bs3wFQSwMEFAAAAAgApY18TQnwH8KeAAAAIQEAACoAAAB0ZXN0cy9Ed3QuSGFiaXRhdC5GZWF0dXJlcy9wYWNrYWdlcy5jb25maWd7v3u/jX1Fbo5CWWpRcWZ+nq2SoZ6BkkJqXnJ+SmZeuq1SaUmaroWSvR0vl01BYnJ2YnpqMZCtoADjKWSm2Cq55ZSm5pU4FhenFpUADSlWQhhnomdoATKxJLEoPbXErSgxN7U8vyjbVikvtcTE1FBJQR/TOL/U8hKgKflpJXpexfl5SKZZ6hnoAfWQZFhoXmYJkhHGeqYE3WOjD/crAFBLAwQUAAAACACljXxN5MqHNqEEAADCDwAANwAAAHRlc3RzL0R3dC5IYWJpdGF0LkZlYXR1cmVzL1N0YW5kYXJkRml4dHVyZXMuRGVzaWduZXIuY3PVV91S20YUvmeGdzjDRWpmQDYQ/kyaGcfGhJlCGdvQzpBcrKVje5vVrrK7suN08mS56CP1FXpWkoUshINbbip+LK3O+fY7/+u/v/9Vr+++6LW5Ua/DGxZbtTtGiZpZDN4mi+4aTLgBXwUIM2YgF4DhHBhYpYSXi/ZiaXmIcIfacCWbr72Gd9A42Dv1Xu83Gg0nl8u2J0yO0RACWLfDiAuEkM3BZ7FB4NJXWqNvYYgTNuVKA5MBzLgQtAJCGQt8lKPZCaYcCUljTjKl9qZeYdtLu3BzQ7IQTcR8hM7Meu/ZkFtmvS4yG2sy9M/NDcc0NlyOoT83FsOzdKn4v+74mjgMmZ6/fVgCaIGxWsmxmO/aeUT+J0wVa9rNF8yYHRiRi4RSnxx8HNGtzwT/SoKkR2skgdb3CrvUy9tkoXZwSayXveYC7tzcz2gMHIteRuJdzEWAOkdKQaacZTkCgn9CIOkLlEBE77iJmSCsOOAqJwUDBSwInIDGUE2RtEMMh6iJfMAtzGkz8Ajm9zRfiI8kUR3LDDtHmnE7SejWyXxQkaV83EmBh45rCnXXh0irPyjNMg73Y6GGTDSbaYC8NuVUR7nPMKINtXexcId707Lk2mFssbaVyS/8YbwBWW28Vc7a2oEtVyH0s7X9sXr/DmdjSbnOfeN1iPl4jPpayVuDepnAUwBZSeYG9FFPueO3WMgNeozFpUUtKUxpNPuWKpDpoMu/LGX0Q/K6K9J8SnCUdYxYQ5lO7p/F3RWTjIzK05mez9bHvUge+Vd6p6TXjoVjeClHKsfN1iqxV3k9cTM5YW44hTOOIgI0V/RHrAvxv+K+VkaNrHeDmkoxZNJHF+F2a+9kb6/Zmioe3EqqSYHBTWqLg84jv+Twsqtr20Vnf6uyoaJxPDSPHhKMNGmbZP6Eqrnsfy7Js8SZGtSi1hfNIKuNpxpHdd2EkZIo7RXZKLxzKl6l32k1M2woCn5bS428YtFrBVPHM6h03LpZV34ueNldY7TlpWS7EdTUMGkbPRyhRuJz/pkamqkV0ngHZCzE9lLolsCfS5LeRvAzSJw9W6e2VTWDvHJeUYK6YaJGtfKbba9lDLVeMd8ulEzxKlhK5BzHCsFCqj7ouVx8ot5LOutn+q9T1JoHmOV6TGcIaeleIwt+MtBOF24vs3bg2n+E2s6T8UnFWcbLp6wbrXFksvmdFMdiIEPVQP7fF83TLXXhu+cVSynej/twRZ6Yp6CWMCjtpkzE+JL58wuFmYIc0cmjfIICw0MuGNWjgvuyXpFutuKuV59jZc+u6WSY3jUXS+3mhw/p3c4KxQ4NCnRnh1z74IfiXTd8bGm760G3/+PtLk2PymSe61od4yr51pSRQyjJuhqx746+uerhyeHe6f7R6cHx0SqEgbJM/DftPoUoVzw5PDjePz08OjreX6XYU8p2uPt6ofSDudUhrL7+dWBXwXVpXrww5A1z/S4HdBNpfZDzL9xY84y0WAXifP7qkWFwT0VtQY0WRea+eNAGdFKiM+nHVO7sGc203M0yOBqDv+Gwsb9esypNUzr1WzrJE14yVxNAGp2ldlQelI/7EH3Q7z9QSwMEFAAAAAgApY18TW5eA767BwAAbiQAADAAAAB0ZXN0cy9Ed3QuSGFiaXRhdC5GZWF0dXJlcy9TdGFuZGFyZEZpeHR1cmVzLnJlc3jtWtluGzcUfS+Qf2DnoQtgzaJdgqQgsWwgaOIElpoEifNAjWhrmhlSITmW1KJf1od+Un+hl+SMNBot1pY0AWoDWoaX5y68PPeS9j9//d16PI1CdE+4CBhtW57tWohQnw0Dete2YnlbqFuPO4++a3HGJLwj1Pq+UEDqA0IvAp8zwW4luibiLer5IxLhZMy8vjbAqGi72cf9EUFjHkSYz9Adw6FA7BbJUSDQLeMRlgg+SYZwGLIJwkgE0Tgk6O2L5+m4gZEjIxoxIcMZGsURpogTPMSDkNhayx2hhGOpbMB0iMYY7KF3Rh1JYO4xD1gs0BBLjORsTATCnKAhowSkOIvvRlq6D0PnjEKwJOHID7EQIGow4DPzAyzJEE0CaeQXeHbW+YspVu40s89s20Z4yGxKpKPcHIETEDr0AxImqCBgJFucCDOKKI5I24LvURARpcfqSDKVTpQuSwHGpi1nPqOzASJZfqsDy7SDuIow4VanNxOSRDasPYu5D16qLEi/XWuhM5QIvQnokE2EfQnrJ86UOzsomvBAPqjojRbaT5FeGaPjCl49q9O6x2FMOjoHVUbNUMggT4TkkC4tx4y2fBZFhMq5GEbJk5aTDrUcBb5G0TkLGfcsnRBtK7G2y/EEFNh6cO5D8tTqPAWtGwGfBjLCY0BMl79t4fE4DHyd7s60MM8DlVY2G/xGfGkPAgq7zh5gQaplK4HV0MbH92ZEgxomgJQWhAc4DH6Hj/bVRR9dcrBgwvhHZFA/pBFKrNxk8jOf0U0hUGMrEdjTt5kkmHO8t3t6lYE5xpApsIjqC0YKDWk4zTopZ+wSAK0wzYj+5mRZE67sz5wsgY0UI2E6QzSOBrBBwBhrntYW4pDyhg4hihIHNOVMA6GibwwEBgx4jo+wPzJcBSjpfGWumnWmaVNPNYSqZICwWVYwYWNYJsT4fMlAXj3xGQdDx4wODaebAGr2NBaLeDxmfM7pisGMqaAhYaYtNIy5PwIC8GXMQaPBOE+oWcMDjf+4UKJ3roplJqWVh0IynuVuA5S6AtJyKWYqEvNBQIwFTIYsycKazAAOUviShFDlFsirVIlGEFkI0JCMldNC2QrxNJmN0iTyY4gmVdWOsjRkEDFCRaBrHtAehPguuCd0YaACMiGNYsAdaH8Q9mFpVJ0PZ0u16IpB3hfQvlyiazYoWq3Q6ukqY0OswzCtz5Bn4D6BNdXSpsIgH8q5wVEPdPonm9AoESiEOEGoBwQahaUFSn1v7u1H2hOocCHU1Ett5DLRmy+yTpjlndtMSew6BiqJiN1LxLUNujBhCQEQ9lOj17zNn+fRdPqMYD1TutI6k6CnvdrhzguGx1/e9Z7Sql6OdvuQ9c7Vif38DqimsUx5yJse6x4Tp8E4ZxHQH+zaF+BHaC/x1zGLXSiYhnwqhs2kTQyG0J5Bp24haOupaFvJhybItK2RlOOm40wmE3tSshm/c4qu66mG07TuqXAkVDmA9p/TBFgsAlmA4tUEuYKRSmustgJqjiJZVTjEGPtknUrQ5niNRt2ZS1nIyYKQkKjimDab2hujq/lMdOGtR2TbkjyGfjcNn54IhkHJm6r4ZuuwHhqxwAf6w9OXPlAoBCamAxZTiHC2SVhrASQWznq6DLxWZ0ZAkE8xrNvq6FpdOgfTBklP162J6oNoarm7iFcGy9mqTKuCbcaDQQxpa5SpV0uVLtXSf4oDKIDrVDu7oekDyOHT52eYQyA4uYUZUdjM5dNydDatlRlNFuKhbFCdRTQIZ4dlQ95roBQsjoiaWcKHp58wAF/ZVkiY4aXqZHAINxjrYrdWQdKE76+ieNoNuKo+tyV3cnGvPZlHLJ1km+ZRy1/Xzl0cl76x7P2MqXWacCezdI1dlOQNUKsgiX+60TAPtt5xJTDJhc26C6/FUTx/97P56msJVd+D7YGS3ogtgRxxPZZcm7bLtqt+z9B5HKoDbpuSWHIcnqFX8QC63F/IrM8+Etoe1Gq44leqXqNUJm69sZf16TXb7tZvv3P7vNZnrpO6E/mGDFzg4zmDtC19h8Pv84ny3nz7Y5G1+urPasKH8+bNjXWWGelyOECrnFXDpZURc2bRU6/6l72lqc+EWtqZGlQtanboyT0OQnU1fckJ6Wm+A6lKveI1itVGqVbNCveZxOHOgj04mSiZeqVUKzYq1WqtmJW5hia6Czveh1sObVomDOt/Ngdng/xlHIb7znkFFzFQgGEGhdkPy19M4bgv1oR2g7xyex97LsxFCtCBmrTDhHPY+Oqg2Qdy0nNuHDigkJ+8crVeLTfcarnqlX++cfaE+lX6R6I9x0I+8WHbihXTKsVG2XO9WqVaqhwCljfuMDzNICeybY51vGlP0jKqs6xY3Dbhz+wOe83COCLPMdxEmeQxY6nMWtrp/pe047mQTbV6vVFrFLfzzlbJlHg8t1Ys1erVUrlYPhXz5OOzC/N0vzLm6X5p5ql4xUqt4roVt1E5nnn2R/ufeb4B5nm3lXkqW5gnt12yvHMLfxbagXjyCKuEs1YiJZr84DEEkw/DLgTz7nMTTD6MDzHMuy/MMAXPq5bL5Vqp6qqfoznmILzNLHMCuFMZuLbHKRyNdgrzltmm4B3KNiq5E7559N0H+DehpZNb8mdtOMKpfx76F1BLAwQUAAAACACljXxNKK8eEXMAAAC/AAAANQAAAHRlc3RzL0R3dC5IYWJpdGF0LkZlYXR1cmVzL1Byb3BlcnRpZXMvQXNzZW1ibHlJbmZvLmNze797v75+aXFmXrpCcGVxSWquNS8XqoBeUGpaTmpySWZ+HlgOhKMTi4tTc5NyKq0UHKGskMySnFQNJZfyEj3XvLLMovy83NS8Ej231MSS0qLUYiXNWBwaXVKLk4syC0DmayjhVuacn5eWmV5alIikkJcLAFBLAQIUABQAAAAIAKWNfE3f/D9EfAAAAJoAAAAKAAAAAAAAAAAAAAAAAAAAAAAuZ2l0aWdub3JlUEsBAhQAFAAAAAgApY18TadmRPGNAgAAnQcAAAcAAAAAAAAAAAAAAAAApAAAAGJvYi5wczFQSwECFAAUAAAACACljXxNW7JsltcCAABzCQAADwAAAAAAAAAAAAAAAABWAwAARHd0LkhhYml0YXQuc2xuUEsBAhQAFAAAAAgApY18TcvIVjUZAAAAFwAAAAkAAAAAAAAAAAAAAAAAWgYAAHJlYWRtZS5tZFBLAQIUABQAAAAIAKWNfE22nRm1jwAAAA8BAAAZAAAAAAAAAAAAAAAAAJoGAABzcmMvY29tbW9uL0NvbXBhbnlJbmZvLmNzUEsBAhQAFAAAAAgApY18TbtqZauHAAAA4gAAABkAAAAAAAAAAAAAAAAAYAcAAHNyYy9jb21tb24vUHJvZHVjdEluZm8uY3NQSwECFAAUAAAACACljXxNQO+1hOEAAACWAQAAIgAAAAAAAAAAAAAAAAAeCAAAc3JjL0R3dC5IYWJpdGF0L0R3dC5IYWJpdGF0LmNzcHJvalBLAQIUABQAAAAIAKWNfE3K75D8bAEAAE0DAAAbAAAAAAAAAAAAAAAAAD8JAABzcmMvRHd0LkhhYml0YXQvSGFiaXRhdHMuY3NQSwECFAAUAAAACACljXxNRPJMu4oAAAAfAQAAGgAAAAAAAAAAAAAAAADkCgAAc3JjL0R3dC5IYWJpdGF0L0lTeXN0ZW0uY3NQSwECFAAUAAAACACljXxN9x0tuDsAAABCAAAAHwAAAAAAAAAAAAAAAACmCwAAc3JjL0R3dC5IYWJpdGF0L3BhY2thZ2VzLmNvbmZpZ1BLAQIUABQAAAAIAKWNfE0yUIiZaAEAAJgEAAAaAAAAAAAAAAAAAAAAAB4MAABzcmMvRHd0LkhhYml0YXQvU3lzdGVtcy5jc1BLAQIUABQAAAAIAKWNfE1XTZXmpAAAABsBAAAtAAAAAAAAAAAAAAAAAL4NAABzcmMvRHd0LkhhYml0YXQvSW50ZXJuYWxzL0R5bmFtaWNEcml2ZUluZm8uY3NQSwECFAAUAAAACACljXxNVsbSOpsBAAA8BAAAKgAAAAAAAAAAAAAAAACtDgAAc3JjL0R3dC5IYWJpdGF0L0ludGVybmFscy9EeW5hbWljU3lzdGVtLmNzUEsBAhQAFAAAAAgApY18TeTPZjoGAQAA7AIAADIAAAAAAAAAAAAAAAAAkBAAAHNyYy9Ed3QuSGFiaXRhdC9JbnRlcm5hbHMvRHluYW1pY1N5c3RlbVByb3ZpZGVyLmNzUEsBAhQAFAAAAAgApY18TWlDwg2TAAAA6AAAACcAAAAAAAAAAAAAAAAA5hEAAHNyYy9Ed3QuSGFiaXRhdC9JbnRlcm5hbHMvSURyaXZlSW5mby5jc1BLAQIUABQAAAAIAKWNfE24mH65XQAAAH4AAAArAAAAAAAAAAAAAAAAAL4SAABzcmMvRHd0LkhhYml0YXQvSW50ZXJuYWxzL0lEcml2ZVByb3ZpZGVyLmNzUEsBAhQAFAAAAAgApY18TRz6cSq/AAAATgEAACsAAAAAAAAAAAAAAAAAZBMAAHNyYy9Ed3QuSGFiaXRhdC9JbnRlcm5hbHMvU3RhbmRhcmRTeXN0ZW0uY3NQSwECFAAUAAAACACljXxNvUKnhOQAAABtAgAAJwAAAAAAAAAAAAAAAABsFAAAc3JjL0R3dC5IYWJpdGF0L0ludGVybmFscy9TeXN0ZW1CYXNlLmNzUEsBAhQAFAAAAAgApY18TSQNfqNnAAAAZQAAACoAAAAAAAAAAAAAAAAAlRUAAHNyYy9Ed3QuSGFiaXRhdC9Qcm9wZXJ0aWVzL0Fzc2VtYmx5SW5mby5jc1BLAQIUABQAAAAIAKWNfE22nRm1jwAAAA8BAAAbAAAAAAAAAAAAAAAAAEQWAAB0ZXN0cy9jb21tb24vQ29tcGFueUluZm8uY3NQSwECFAAUAAAACACljXxNvuDs2oAAAADbAAAAGwAAAAAAAAAAAAAAAAAMFwAAdGVzdHMvY29tbW9uL1Byb2R1Y3RJbmZvLmNzUEsBAhQAFAAAAAgApY18TWFROusrAQAA0AIAADYAAAAAAAAAAAAAAAAAxRcAAHRlc3RzL0R3dC5IYWJpdGF0LkZlYXR1cmVzL0R3dC5IYWJpdGF0LkZlYXR1cmVzLmNzcHJvalBLAQIUABQAAAAIAKWNfE3iZhqEpAEAAOkEAAAvAAAAAAAAAAAAAAAAAEQZAAB0ZXN0cy9Ed3QuSGFiaXRhdC5GZWF0dXJlcy9FbnZpcm9ubWVudFRlc3Rlci5jc1BLAQIUABQAAAAIAKWNfE1PI4NmvAEAAF8EAAAwAAAAAAAAAAAAAAAAADUbAAB0ZXN0cy9Ed3QuSGFiaXRhdC5GZWF0dXJlcy9Kc29uU3lzdGVtUHJvdmlkZXIuY3NQSwECFAAUAAAACACljXxNCfAfwp4AAAAhAQAAKgAAAAAAAAAAAAAAAAA/HQAAdGVzdHMvRHd0LkhhYml0YXQuRmVhdHVyZXMvcGFja2FnZXMuY29uZmlnUEsBAhQAFAAAAAgApY18TeTKhzahBAAAwg8AADcAAAAAAAAAAAAAAAAAJR4AAHRlc3RzL0R3dC5IYWJpdGF0LkZlYXR1cmVzL1N0YW5kYXJkRml4dHVyZXMuRGVzaWduZXIuY3NQSwECFAAUAAAACACljXxNbl4DvrsHAABuJAAAMAAAAAAAAAAAAAAAAAAbIwAAdGVzdHMvRHd0LkhhYml0YXQuRmVhdHVyZXMvU3RhbmRhcmRGaXh0dXJlcy5yZXN4UEsBAhQAFAAAAAgApY18TSivHhFzAAAAvwAAADUAAAAAAAAAAAAAAAAAJCsAAHRlc3RzL0R3dC5IYWJpdGF0LkZlYXR1cmVzL1Byb3BlcnRpZXMvQXNzZW1ibHlJbmZvLmNzUEsFBgAAAAAcABwA2wgAAOorAAAAAA==";
            var bytes = Convert.FromBase64String(value);

            var basePathOutput = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N") + ".output"));

            ExtractZip(basePathOutput.FullName, bytes);
        }

//        [Fact]
        [Theory]
        [InlineData("git@bitbucket.org:base2art/cicd.git", "fixture/logmessage-tests", "b93815e369cf5b5d8aa399c1a60fd83f9a6ce1b2", 2, 10)]
        public async void Test3(string cloneLocation, string branch, string hash, int branchCount, int logCount)
        {
            var path = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N"));
            try
            {
                var dataStore = new Base2art.DataStorage.InMemory.DataDefiner(true);
                var store = new DataStore(dataStore);
                var dbms = new Dbms(dataStore);

                await dbms.CreateTable<node_source_control_v1>().Execute();

                var service = new GitCheckoutService(new DefinableDirectoryProvider(path), false, new ProcessBuilderFactory(), new WrapperDD(store));
                var branches = await service.GetBranches(new GitRepositoryData
                                                         {
                                                             CloneLocation = cloneLocation
                                                         }, ClaimsPrincipal.Current);

                branches.Length.Should().BeGreaterOrEqualTo(branchCount);
                var item = branches.First(x => x.Name == branch);

                item.Hash.Should().Be(hash);

                var branchHistory = await service.GetLogs(new GitRepositoryBranchData
                                                          {
                                                              Repository = new GitRepositoryData
                                                                           {
                                                                               CloneLocation = cloneLocation,
                                                                           },
                                                              Branch = new IsolatedBranchData
                                                                       {
                                                                           Name = item.Name,
                                                                           Hash = item.Hash
                                                                       }
                                                          });

                branchHistory.Logs.Length.Should().Be(logCount);
                // 2019-04-18T11:21:09
                this.helper.WriteLine(branchHistory.Logs[0].When.ToString("yyyy-MM-ddTHH:mm:ss"));
                branchHistory.Logs[0].When.Should().Be(DateTimeOffset.Parse("2019-04-18T16:21:09-07:00").ToUniversalTime().DateTime);
                branchHistory.Logs[0].FilesChanged.Length.Should().Be(1);
                branchHistory.Logs[0].FilesChanged[0].Should().Be(".artifacts");

//                branchHistory.Logs[2].When.Should().Be(DateTimeOffset.Parse("2019-04-18T16:21:09-07:00").ToUniversalTime().DateTime);
                branchHistory.Logs[2].FilesAdded.Length.Should().Be(6);
                branchHistory.Logs[2].FilesRemoved.Length.Should().Be(29);
                branchHistory.Logs[2].FilesChanged.Length.Should().Be(38);
                branchHistory.Logs[2].FilesChanged[0].Should().Be(".bob/116-package.ps1");
            }
            finally
            {
                try
                {
                    Directory.Delete(path, true);
                }
                catch (Exception)
                {
                }
            }
        }

        public class WrapperDD : IDataStoreFactory
        {
            private readonly IDataStore dataStoreFactory;

            public WrapperDD(IDataStore dataStoreFactory)
            {
                this.dataStoreFactory = dataStoreFactory;
            }

            public IDataStore Create(string name)
            {
                return this.dataStoreFactory;
            }
        }
    }
}