﻿namespace Base2art.CiCd.SourceControl.Web.Git
{
    using System.IO;

    public class DirectoryProvider : DefinableDirectoryProvider, IDirectoryProvider
    {
        public DirectoryProvider() : base("/home/tyoung/.local/share/base2art/cicd/source-node")
        {
        }
    }
}