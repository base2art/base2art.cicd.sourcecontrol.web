﻿namespace Base2art.CiCd.SourceControl.Web.Git
{
    using System;

    public static class Trier
    {
        public static void Try(this Action action, int times, bool swallow)
        {
            Try<Exception>(action, 0, times, swallow);
        }

        public static void Try<T>(this Action action, int times, bool swallow)
            where T : Exception
        {
            Try<T>(action, 0, times, swallow);
        }

        private static void Try<T>(Action action, int current, int times, bool swallow) where T : Exception
        {
            try
            {
                action();
            }
            catch (T)
            {
                if (current <= times)
                {
                    Try<T>(action, current + 1, times, swallow);
                    return;
                }

                if (swallow)
                {
                    return;
                }

                throw;
            }
        }
    }
}