﻿namespace Base2art.CiCd.SourceControl.Web.Git
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Base2art.Diagnostic;

    public static class AllPaths
    {
        public static Task<FileInfo> GitExecutable() => Environments.FindFileInPath("git", "git.exe");

//        public static bool IsRunningOnUnix
//        {
//            get
//            {
//                return Platform.OperatingSystem == OperatingSystemType.MacOSX ||
//                       Platform.OperatingSystem == OperatingSystemType.Unix;
//            }
//        }
        public static Task<IProcessBuilder> CreateAsUser(this IProcessBuilderFactory factory)
        {
            try
            {
                return Task.FromResult(factory.Create().WithLoadedUserProfile());
            }
            catch (PlatformNotSupportedException)
            {
                return Task.FromResult(factory.Create());
            }
        }

        public static async Task<IProcessBuilder> Git(this IProcessBuilderFactory factory)
        {
            return (await factory.CreateAsUser())
                .WithExecutable(await GitExecutable());
        }
    }
}

//            var home = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

//                          .WithEnvironmentVariable("USERNAME", Environment.UserName)
//                          .WithEnvironmentVariable("USERPROFILE", home)
//                          .WithEnvironmentVariable("HOMEPATH", new string(home.SkipWhile(x => x != ':').Skip(1).ToArray()))