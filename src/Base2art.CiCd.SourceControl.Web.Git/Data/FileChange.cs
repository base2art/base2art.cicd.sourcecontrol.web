namespace Base2art.CiCd.SourceControl.Web.Git.Data
{
    internal class FileChange
    {
        public string Commit { get; set; }
        public string File { get; set; }
        public FileChangeType ChangeType { get; set; }
    }
}