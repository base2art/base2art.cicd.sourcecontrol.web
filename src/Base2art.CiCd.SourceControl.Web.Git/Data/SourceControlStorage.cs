﻿namespace Base2art.CiCd.SourceControl.Web.Git.Data
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Resources;

    public class SourceControlStorage : ISourceControlStorage
    {
        private readonly IDataStore store;

        public SourceControlStorage(IDataStoreFactory factory) => this.store = factory.Create("source");

        public Task SetStatus(Guid id, Guid processId, IsolatedCheckoutState state)
        {
            return this.store.Insert<node_source_control_v1>()
                       .Record(rs => rs.Field(x => x.id, id.ToString("N"))
                                       .Field(x => x.process_id, processId.ToString("N"))
                                       .Field(x => x.state, (int) state)
                                       .Field(x => x.date, DateTime.UtcNow))
                       .Execute();
        }

        public async Task<IsolatedCheckoutState> GetStatus(Guid id, Guid processId)
        {
            var item = await this.store.SelectSingle<node_source_control_v1>()
                                 .Where(rs => rs.Field(x => x.id, id.ToString("N"), (x, y) => x == y)
                                                .Field(x => x.process_id, processId.ToString("N"), (x, y) => x == y))
                                 .OrderBy(rs => rs.Field(x => x.date, ListSortDirection.Descending))
                                 .Execute();

            if (item == null)
            {
                return IsolatedCheckoutState.Unknown;
            }

            return (IsolatedCheckoutState) item.state;
        }
    }
}