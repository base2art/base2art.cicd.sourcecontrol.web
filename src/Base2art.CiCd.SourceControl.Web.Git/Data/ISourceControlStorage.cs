namespace Base2art.CiCd.SourceControl.Web.Git.Data
{
    using System;
    using System.Threading.Tasks;
    using Resources;

    public interface ISourceControlStorage
    {
        Task SetStatus(Guid id, Guid processId, IsolatedCheckoutState state);

        Task<IsolatedCheckoutState> GetStatus(Guid id, Guid processId);
    }
}