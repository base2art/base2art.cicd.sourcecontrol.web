namespace Base2art.CiCd.SourceControl.Web.Git.Data
{
    public class Author
    {
        public string name { get; set; }
        public string email { get; set; }
        public string date { get; set; }
    }
}