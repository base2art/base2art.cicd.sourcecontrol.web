namespace Base2art.CiCd.SourceControl.Web.Git.Data
{
    using System;

    public interface node_source_control_v1
    {
        string id { get; }
        string process_id { get; }
        int state { get; }
        DateTime date { get; }
    }
}