namespace Base2art.CiCd.SourceControl.Web.Git.Data
{
    public enum FileChangeType
    {
        Modified,
        Deleted,
        Added
    }
}