namespace Base2art.CiCd.SourceControl.Web.Git.Data
{
    public class GitLog
    {
        public string commit { get; set; }
        public string abbreviated_commit { get; set; }
        public string tree { get; set; }
        public string abbreviated_tree { get; set; }
        public string parent { get; set; }
        public string abbreviated_parent { get; set; }
        public string refs { get; set; }
        public string encoding { get; set; }
        public string subject { get; set; }
        public string sanitized_subject_line { get; set; }
        public string body { get; set; }
        public string commit_notes { get; set; }
        public string verification_flag { get; set; }
        public string signer { get; set; }
        public string signer_key { get; set; }
        public Author author { get; set; }
        public Commiter commiter { get; set; }
    }
}