namespace Base2art.CiCd.SourceControl.Web.Git
{
    using System;
    using System.IO;

    public class DefinableDirectoryProvider : IDirectoryProvider
    {
        private readonly DirectoryInfo info;
        private readonly DirectoryInfo data;

        public DefinableDirectoryProvider(string path)
        {
            if (string.IsNullOrWhiteSpace(path) || path.StartsWith("#"))
            {
                path = Path.Combine(
                                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                    "base2art/cicd/source-node");
            }
            
            this.info = Directory.CreateDirectory(Path.Combine(path, "source/wd"));
            this.data = Directory.CreateDirectory(Path.Combine(path, "Data"));
        }

        public DirectoryInfo OriginsRoot() => Directory.CreateDirectory(Path.Combine(this.GetWorkingDir(), "bo-04fdc93275a3"));

        public DirectoryInfo DataDirectory() => this.data;

        public string GetWorkingDir() => this.info != null ? this.info.FullName : Path.GetTempPath();

        public DirectoryInfo GetCheckoutRoot() => Directory.CreateDirectory(Path.Combine(this.GetWorkingDir(), "bb-27D5D0B00578"));
    }
}