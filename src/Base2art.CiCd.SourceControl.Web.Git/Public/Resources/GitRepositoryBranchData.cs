namespace Base2art.CiCd.SourceControl.Web.Git.Public.Resources
{
    using SourceControl.Resources;

    public class GitRepositoryBranchData
    {
        public GitRepositoryData Repository { get; set; }
        public IsolatedBranchData Branch { get; set; }
//        public string CloneLocation { get; set; }
//        public string BranchName { get; set; }
//        public string BranchHash { get; set; }
    }
}