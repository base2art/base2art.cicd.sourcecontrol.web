﻿namespace Base2art.CiCd.SourceControl.Web.Git.Public.Resources
{
    public class GitCloneData
    {
        public GitRepositoryData Repository { get; set; }
        public string BranchOrTag { get; set; }
    }
}

//        public static Guid Id { get; } = new Guid("3862B425-EF36-4B28-A57B-6DA412F49B79");

//        public static SourceControlInfo Create(string location, string file, string[] arguments)
//            => new SourceControlInfo
//               {
//                   Id = new Guid("2cc123b5-11f3-47e9-a029-78e1d9eaf1b2"),
//                   SourceControlType = Id,
//                   SourceControlData = new DataWithKey {CloneLocation = location}.ToDictionary(),
//                   BuildInstructions = new[]
//                                       {
//                                           new BuildInstructionInfo
//                                           {
//                                               ExecutableFile = file,
//                                               ExecutableArguments = arguments
//                                           }
//                                       }
//               };

//        public Dictionary<string, string> ToDictionary()
//        {
//            return new Dictionary<string, string>
//                   {
//                       {nameof(this.CloneLocation), this.CloneLocation.ToString()}
//                   };
//        }