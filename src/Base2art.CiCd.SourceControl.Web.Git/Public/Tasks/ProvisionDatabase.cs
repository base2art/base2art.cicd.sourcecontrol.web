namespace Base2art.CiCd.SourceControl.Web.Git.Public.Tasks
{
    using Base2art.DataStorage;
    using Data;
    using Servers;

    public class ProvisionDatabaseTask
    {
        private readonly IDbms dbms;
        private readonly IDataStore store;

        public ProvisionDatabaseTask(IDbmsFactory dbmsFactory, IDataStoreFactory store)
        {
            this.dbms = dbmsFactory.Create("source");
            this.store = store.Create("source");
        }

        public void ExecuteAsync()
        {
            this.dbms.CreateTable<node_source_control_v1>().Execute().RunAway();
        }
    }
}