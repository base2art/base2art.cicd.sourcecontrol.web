namespace Base2art.CiCd.SourceControl.Web.Git.Public.Endpoints
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Resources;
    using Servers;

    public class SupportedFeatureService
    {
        private readonly IRequireAuthProvider requireAuthentication;

        public SupportedFeatureService(IRequireAuthProvider requireAuthentication)
        {
            this.requireAuthentication = requireAuthentication;
        }

        public Task<SupportedFeature[]> GetSupportedCheckoutTypes(ClaimsPrincipal principal)
        {
            if (this.requireAuthentication.Value)
            {
                principal.RequireSignedIn();
            }

            return Task.FromResult(new[]
                                   {
                                       new SupportedFeature
                                       {
                                           Id = new Guid("DB7AB9E6-8258-4FCE-91D0-851CDCCFF8CC"),
                                           FullName = typeof(GitRepositoryData).FullName,
                                           Name = nameof(GitRepositoryData),
                                           DefaultData = new GitRepositoryData
                                                         {
                                                             CloneLocation =
                                                                 "git@gitlab.com:base2art/cicd/sample-project.git"
                                                         }
                                       }
                                   });
        }
    }
}