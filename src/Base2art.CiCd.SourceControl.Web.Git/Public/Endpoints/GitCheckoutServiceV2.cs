namespace Base2art.CiCd.SourceControl.Web.Git.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using DataStorage;
    using Diagnostic;
    using Data;
    using Hashing;
    using LibGit2Sharp;
    using Resources;
    using Servers;
    using SourceControl.Resources;

    public class GitCheckoutServiceV2 : ICheckoutPhaseService
    {
        private readonly IDirectoryProvider directoryProvider;
        private readonly IRequireAuthProvider authentication;
        private readonly IProcessBuilderFactory processBuilderFactory;
        private readonly ISourceControlStorage storage;
        private static Guid BackingProcessId;

        public GitCheckoutServiceV2(
            IDirectoryProvider directoryProvider,
            IRequireAuthProvider authentication,
            IProcessBuilderFactory processBuilderFactory,
            IDataStoreFactory factory)
        {
            this.authentication = authentication;
            this.processBuilderFactory = processBuilderFactory;
            this.storage = new SourceControlStorage(factory);
            this.directoryProvider = directoryProvider;
        }

        private static Guid ProcessId
        {
            get
            {
                if (BackingProcessId == Guid.Empty)
                {
                    BackingProcessId = Guid.NewGuid();
                }

                return BackingProcessId;
            }
        }

        Task<IsolatedBranchData[]> ICheckoutPhaseService.GetBranches(Dictionary<string, string> projectSourceControlData, ClaimsPrincipal principal)
            => this.GetBranches(Convert(projectSourceControlData), principal);

        Task<IsolatedBranchLogData> ICheckoutPhaseService.GetLogs(
            RepositoryBranchData<Dictionary<string, string>> projectSourceControlData,
            ClaimsPrincipal principal)
            => this.GetLogs(new GitRepositoryBranchData
                            {
                                Repository = Convert(projectSourceControlData.Repository),
                                Branch = new IsolatedBranchData
                                         {
                                             Hash = projectSourceControlData.Branch.Hash,
                                             Name = projectSourceControlData.Branch.Name
                                         }
                            }, principal);

        Task<IsolatedCheckout> ICheckoutPhaseService.StartCheckout(
            Dictionary<string, string> projectSourceControlData,
            string branchOrTag,
            ClaimsPrincipal principal)
            => this.StartCheckout(new GitCloneData { Repository = Convert(projectSourceControlData) }, principal);

        Task<SupportedFeature[]> ISupportedFeatureService.GetSupportedFeatures(ClaimsPrincipal principal)
            => new SupportedFeatureService(this.authentication).GetSupportedCheckoutTypes(principal);

        public Task RemoveSource(Guid checkoutId, ClaimsPrincipal principal)
        {
            this.authentication.RequireSignedIn(principal);

            return Task.Factory.StartNew(() =>
            {
                var path = this.directoryProvider.GetCheckoutRoot();

                var destinationPath = Path.Combine(path.FullName, checkoutId.ToString("N"));
                new Action(() => DirectoryDeleteRecursive(destinationPath)).Try(3, true);
            });
        }

        public async Task<IsolatedBranchData[]> GetBranches(GitRepositoryData projectSourceControlData, ClaimsPrincipal principal)
        {
            this.authentication.RequireSignedIn(principal);

            var cloneLocation = projectSourceControlData?.CloneLocation;
            if (string.IsNullOrWhiteSpace(cloneLocation))
            {
                throw new ArgumentNullException(nameof(projectSourceControlData) + "." + nameof(cloneLocation));
            }

            using (var repo = await this.GetNonWorkingRepository(cloneLocation, true))
            {
                var mappedBranches = repo.Branches
                                         .Where(x => x.IsRemote)
                                         .Where(x => !x.Reference.TargetIdentifier.StartsWith("refs/remotes/origin/"))
                                         .Select(x => new IsolatedBranchData
                                                      {
                                                          Hash = x.Reference.TargetIdentifier,
                                                          Name = this.Name(x)
                                                      })
                                         .ToArray();
                return mappedBranches;
            }
        }

        public Task<IsolatedCheckout> StartCheckout(GitCloneData projectSourceControlData, ClaimsPrincipal principal)
        {
            this.authentication.RequireSignedIn(principal);

            var id = Guid.NewGuid();
            this.StartCheckoutInternal(id, projectSourceControlData).RunAway();

            return Task.FromResult(new IsolatedCheckout
                                   {
                                       Id = id,
                                       State = IsolatedCheckoutState.Pending
                                   });
        }

        public async Task<IsolatedCheckout> GetCheckoutStatus(Guid checkoutId, ClaimsPrincipal principal)
        {
            this.authentication.RequireSignedIn(principal);

            var item = await this.storage.GetStatus(checkoutId, ProcessId);

            return new IsolatedCheckout
                   {
                       Id = checkoutId,
                       State = item
                   };
        }

        public async Task<IsolatedSourceData> GetSourceWithMetaData(Guid checkoutId, ClaimsPrincipal principal)
        {
            this.authentication.RequireSignedIn(principal);

            var path = this.directoryProvider.GetCheckoutRoot();
            var codePath = Path.Combine(path.FullName, checkoutId.ToString("N"));

            if (!Directory.Exists(codePath))
            {
                throw new Base2art.Web.Exceptions.NotFoundException();
            }

            var destinationPath = Directory.CreateDirectory(codePath);

            var content = await destinationPath.ZipToBytes();
            return new IsolatedSourceData
                   {
                       Content = content,
                       RequiredFeatureIds = new Guid[0]
                   };
        }

        public async Task<byte[]> GetSource(Guid checkoutId, ClaimsPrincipal principal)
        {
            this.authentication.RequireSignedIn(principal);

            var result = await this.GetSourceWithMetaData(checkoutId, principal);
            return result.Content;
        }

        // Dictionary<string, string> projectSourceControlData, ClaimsPrincipal principal
        public async Task<IsolatedBranchLogData> GetLogs(GitRepositoryBranchData data, ClaimsPrincipal principal)
        {
            this.authentication.RequireSignedIn(principal);

            using (var repo = await GetNonWorkingRepository(data.Repository.CloneLocation, false))
            {
                var branch = repo.Branches.FirstOrDefault(x => x.CanonicalName == $"refs/remotes/origin/{data.Branch.Name}");

                IEnumerable<Commit> branchCommits = branch?.Commits;
                var branchCommits2 = (branchCommits ?? new Commit[0]).Select(x => (x, this.GetCommit(repo, x.Tree, x.Parents))).ToArray();
                return new IsolatedBranchLogData
                       {
                           Name = data.Branch.Name,
                           Hash = data.Branch.Hash,
                           Logs = branchCommits2.Select(x => new IsolatedCheckoutLogData
                                                             {
                                                                 Id = x.Item1.Sha,
                                                                 Author = x.Item1.Author.Name,
                                                                 Message = x.Item1.Message,
                                                                 When = x.Item1.Author.When.UtcDateTime,
                                                                 FilesAdded = x.Item2.added,
                                                                 FilesChanged = x.Item2.modified,
                                                                 FilesRemoved = x.Item2.remove
                                                             }).ToArray()
                       };
            }

//            return returnValue;
        }

        private static GitRepositoryData Convert(Dictionary<string, string> projectSourceControlData)
            => new GitRepositoryData { CloneLocation = projectSourceControlData["CloneLocation"] };

        private (string[] added, string[] remove, string[] modified) GetCommit(Repository repo, Tree sha, IEnumerable<Commit> parents)
        {
//            Tree commitTree = repo.Head.Tip.Tree; // Main Tree
            Tree parentCommitTree = parents?.FirstOrDefault()?.Tree; // Secondary Tree

            var patch = repo.Diff.Compare<Patch>(parentCommitTree, sha); // Difference

            HashSet<string> added = new HashSet<string>();
            HashSet<string> remove = new HashSet<string>();
            HashSet<string> modified = new HashSet<string>();
            foreach (var ptc in patch)
            {
                if (ptc.Status == ChangeKind.Added)
                {
                    added.Add(ptc.Path);
                }
                else if (ptc.Status == ChangeKind.Deleted)
                {
                    remove.Add(ptc.Path);
                }
                else
                {
                    modified.Add(ptc.Path);
                }
            }

            return (added.ToArray(), remove.ToArray(), modified.ToArray());
        }

        private string Name(Branch branch)
        {
            return branch.CanonicalName.Replace("refs/remotes/origin/", "");
        }

        private async Task<Repository> ProcessNew(string path, string cloneLocation, string latestKeyPath, string legacyKeyPath)
        {
            var key = Guid.NewGuid().ToString("N");
            var newPath = Path.Combine(path, key);
            Directory.CreateDirectory(path);

            var git = await this.processBuilderFactory.Git();
            await git.WithWorkingDirectory(path)
                     .WithArguments(new[] { "clone", cloneLocation.Trim(), key })
                     .WithErrorWriter(Console.Out)
                     .Execute();

            if (File.Exists(latestKeyPath))
            {
                try
                {
                    CleanRepos(path, legacyKeyPath);
                }
                catch (IOException ioe)
                {
                    Console.WriteLine(ioe);
                }
                catch (Exception ioe)
                {
                    Console.WriteLine(ioe);
                }

                File.Copy(latestKeyPath, legacyKeyPath, true);
            }

            File.WriteAllText(latestKeyPath, key);

            var repo = new Repository(newPath);
            return repo;
        }

        private static void CleanRepos(string path, string legacyKeyPath)
        {
            if (File.Exists(legacyKeyPath))
            {
                var legacyKey = File.ReadAllText(legacyKeyPath).Trim();
                if (Guid.TryParse(legacyKey, out var id))
                {
                    var newPath2 = Path.Combine(path, id.ToString("N"));
                    if (Directory.Exists(newPath2))
                    {
                        DirectoryDeleteRecursive(newPath2);
                    }
                }
            }
        }

        private static void DirectoryDeleteRecursive(string directory)
        {
            foreach (string subdirectory in Directory.EnumerateDirectories(directory))
            {
                DirectoryDeleteRecursive(subdirectory);
            }

            foreach (string fileName in Directory.EnumerateFiles(directory))
            {
                var fileInfo = new FileInfo(fileName)
                               {
                                   Attributes = FileAttributes.Normal
                               };
                fileInfo.Delete();
            }

            Directory.Delete(directory);
        }

        private async Task<Repository> GetNonWorkingRepository(string cloneLocation, bool useNew)
        {
            var localPath = $"v2-{cloneLocation.Trim()}".HashAsGuid().ToString("N");
            var path = Path.Combine(this.directoryProvider.OriginsRoot().FullName, localPath);
            Directory.CreateDirectory(path);

            var latestKeyPath = Path.Combine(path, "latest");
            var legacyKeyPath = Path.Combine(path, "legacy");

            if (useNew)
            {
                return await this.ProcessNew(path, cloneLocation, latestKeyPath, legacyKeyPath);
            }

            if (!File.Exists(latestKeyPath))
            {
                return await this.ProcessNew(path, cloneLocation, latestKeyPath, legacyKeyPath);
            }

            var key = File.ReadAllText(latestKeyPath);
            var gitPath = Path.Combine(path, key.Trim(), ".git");
            if (!Directory.Exists(gitPath))
            {
                return await this.ProcessNew(path, cloneLocation, latestKeyPath, legacyKeyPath);
            }

            var git = await this.processBuilderFactory.Git();
            var workingDir = Path.Combine(path, key.Trim());
            await git.WithWorkingDirectory(workingDir)
                     .WithArguments(new[] { "pull" })
                     .WithErrorWriter(Console.Out)
                     .Execute();

            var repository = new Repository(workingDir);
            return repository;
        }

        private async Task StartCheckoutInternal(Guid id, GitCloneData cloneData)
        {
            await this.storage.SetStatus(id, ProcessId, IsolatedCheckoutState.Claimed);
            await this.storage.SetStatus(id, ProcessId, IsolatedCheckoutState.Working);

            await (await this.processBuilderFactory.Git())
                  .WithWorkingDirectory(this.directoryProvider.GetCheckoutRoot().FullName)
                  .WithArguments(new[] { "clone", "--branch", cloneData.BranchOrTag, cloneData.Repository.CloneLocation.Trim(), id.ToString("N") })
                  .WithOutputWriter(Console.Out)
                  .WithErrorWriter(Console.Out)
                  .Execute();

            await this.storage.SetStatus(id, ProcessId, IsolatedCheckoutState.CompletedSuccess);
        }
    }
}