namespace Base2art.CiCd.SourceControl.Web.Git.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.Diagnostic;
    using Data;
    using Resources;
    using Serialization;
    using Serialization.Internals;
    using Servers;
    using SourceControl.Resources;

    public class GitCheckoutService
    {
        private readonly IDirectoryProvider directoryProvider;
        private readonly IProcessBuilderFactory processBuilderFactory;
        private readonly bool requireAuthentication;
        private readonly ISourceControlStorage storage;
        private readonly CamelCasingSimpleJsonSerializer ser = new CamelCasingSimpleJsonSerializer();

        private const int MaxCount = 10;
        private static Guid BackingProcessId;

        public GitCheckoutService(
            IDirectoryProvider directoryProvider,
            bool requireAuthentication,
            IProcessBuilderFactory builderFactory,
            IDataStoreFactory factory)
        {
            this.directoryProvider = directoryProvider;
            this.requireAuthentication = requireAuthentication;
            this.processBuilderFactory = builderFactory;
            this.storage = new SourceControlStorage(factory);
        }

        private static Guid ProcessId
        {
            get
            {
                if (BackingProcessId == Guid.Empty)
                {
                    BackingProcessId = Guid.NewGuid();
                }

                return BackingProcessId;
            }
        }

        public Task RemoveSource(Guid checkoutId, ClaimsPrincipal principal)
        {
            if (this.requireAuthentication)
            {
                principal.RequireSignedIn();
            }

            return Task.Factory.StartNew(() =>
            {
                var path = this.directoryProvider.GetCheckoutRoot();

                var destinationPath = Path.Combine(path.FullName, checkoutId.ToString("N"));
                new Action(() => Directory.Delete(destinationPath, true)).Try(3, true);
            });
        }

        public Task<SupportedFeature[]> GetSupportedCheckoutTypes() => Task.FromResult(new[]
                                                                                       {
                                                                                           new SupportedFeature
                                                                                           {
                                                                                               Id = new Guid("DB7AB9E6-8258-4FCE-91D0-851CDCCFF8CC"),
                                                                                               FullName = typeof(GitRepositoryData).FullName,
                                                                                               Name = nameof(GitRepositoryData),
                                                                                               DefaultData = new GitRepositoryData
                                                                                                             {
                                                                                                                 CloneLocation =
                                                                                                                     "git@gitlab.com:base2art/cicd/sample-project.git"
                                                                                                             }
                                                                                           }
                                                                                       });

        public async Task<IsolatedBranchData[]> GetBranches(GitRepositoryData projectSourceControlData, ClaimsPrincipal principal)
        {
            if (this.requireAuthentication)
            {
                principal.RequireSignedIn();
            }

            return await this.GetBranchesInternal(projectSourceControlData, null);
        }

        public Task<IsolatedCheckout> StartCheckout(GitCloneData projectSourceControlData, ClaimsPrincipal principal)
        {
            if (this.requireAuthentication)
            {
                principal.RequireSignedIn();
            }

            var id = Guid.NewGuid();
            this.StartCheckoutInternal(id, projectSourceControlData).RunAway();

            return Task.FromResult(new IsolatedCheckout
                                   {
                                       Id = id,
                                       State = IsolatedCheckoutState.Pending
                                   });
        }

        public async Task<IsolatedCheckout> GetCheckoutStatus(Guid checkoutId, ClaimsPrincipal principal)
        {
            if (this.requireAuthentication)
            {
                principal.RequireSignedIn();
            }

            var item = await this.storage.GetStatus(checkoutId, ProcessId);

            return new IsolatedCheckout
                   {
                       Id = checkoutId,
                       State = item
                   };
        }

        public Task<byte[]> GetSource(Guid checkoutId, ClaimsPrincipal principal)
        {
            if (this.requireAuthentication)
            {
                principal.RequireSignedIn();
            }

            var path = this.directoryProvider.GetCheckoutRoot();

            var destinationPath = Directory.CreateDirectory(Path.Combine(path.FullName, checkoutId.ToString("N")));

            return destinationPath.ZipToBytes();
        }
/*

        private IKeyedOriginRunner Runner(GitRepositoryData projectSourceControlData)
            => new KeyedOriginRunner(this.processBuilderFactory, this.directoryProvider, projectSourceControlData, this.storage, ProcessId);
*/

        private async Task StartCheckoutInternal(Guid id, GitCloneData cloneData)
        {
            await this.storage.SetStatus(id, ProcessId, IsolatedCheckoutState.Claimed);

//            var runner = this.Runner(new GitRepositoryData {CloneLocation = cloneData.Repository.CloneLocation});
//            var state = await runner.GetOrigin();
//            while (!state.In(IsolatedCheckoutState.CompletedFail, IsolatedCheckoutState.CompletedSuccess))
//            {
//                await Task.Delay(TimeSpan.FromSeconds(3));
//                state = await runner.GetOrigin();
//            }

//            await runner.Pull();

            await this.storage.SetStatus(id, ProcessId, IsolatedCheckoutState.Working);

            var destinationPath = Path.Combine(this.directoryProvider.GetCheckoutRoot().FullName, id.ToString("N"));
            await this.CloneAsync(cloneData.Repository.CloneLocation.Trim(), destinationPath, cloneData.BranchOrTag);

            await this.storage.SetStatus(id, ProcessId, IsolatedCheckoutState.CompletedSuccess);
        }

        private async Task<IsolatedBranchData[]> GetBranchesInternal(GitRepositoryData cloneData, string checkoutLocation)
        {
            using (var sb = new StringWriter())
            {
                using (var sbError = new StringWriter())
                {
                    if (string.IsNullOrWhiteSpace(checkoutLocation))
                    {
                        await (await this.processBuilderFactory.Git())
                              .WithWorkingDirectory(checkoutLocation)
                              .WithArguments(new[] {"ls-remote", "-q", cloneData.CloneLocation})
                              .WithOutputWriter(sb)
                              .WithErrorWriter(sbError)
                              .Execute();
                    }
                    else
                    {
                        await (await this.processBuilderFactory.Git())
                              .WithWorkingDirectory(checkoutLocation)
                              .WithArguments("ls-remote -q")
                              .WithOutputWriter(sb)
                              .WithErrorWriter(sbError)
                              .Execute();
                    }

                    await sb.FlushAsync();
                    await sbError.FlushAsync();

                    var content = sb.GetStringBuilder().ToString();

                    var lines1 = content.Split('\n', '\r');
                    var lines = lines1.Select(x => x.Trim())
                                      .Where(x => !string.IsNullOrWhiteSpace(x))
                                      .Where(x => !x.Contains("->"))
                                      .Select(x => x.Split(new[] {' ', '\t'}, StringSplitOptions.RemoveEmptyEntries))
                                      .Where(x => x.Length == 2)
                                      .Where(x => x[1].StartsWith("refs/heads/"))
                                      .Select(x => new IsolatedBranchData
                                                   {
                                                       Name = x[1].Substring("refs/heads/".Length).Trim(),
                                                       Hash = x[0].Trim(),
//                                                       Logs = new IsolatedCheckoutLogData[0]
                                                   })
                                      .ToArray();

                    return lines;
                }
            }
        }

        private async Task<List<GitLog>> GetLogItems(IsolatedBranchData line, string checkoutLocation)
        {
            using (var sbi = new StringWriter())
            {
                using (var sbErrori = new StringWriter())
                {
                    // ::__s__::
                    // ::__e__::

                    var args = new List<string>();

                    args.Add("log");
                    if (!string.IsNullOrWhiteSpace(line.Hash))
                    {
                        args.Add(line.Hash);
                    }

                    args.Add("--date=format:%Y-%m-%d %H:%M:%S");
                    args.Add("--max-count=" + MaxCount);
                    args.Add("--pretty=format:{%n"
                             + "  'commit': ::__s__::%H::__e__::,%n"
                             + "  'abbreviated_commit': ::__s__::%h::__e__::,%n"
                             + "  'tree': ::__s__::%T::__e__::,%n"
                             + "  'abbreviated_tree': ::__s__::%t::__e__::,%n"
                             + "  'parent': ::__s__::%P::__e__::,%n"
                             + "  'abbreviated_parent': ::__s__::%p::__e__::,%n"
                             + "  'refs': ::__s__::%D::__e__::,%n"
                             + "  'encoding': ::__s__::%e::__e__::,%n"
                             + "  'subject': ::__s__::%s::__e__::,%n"
                             + "  'sanitized_subject_line': ::__s__::%f::__e__::,%n"
                             + "  'body': ::__s__::%b::__e__::,%n"
                             + "  'commit_notes': ::__s__::%N::__e__::,%n"
                             + "  'verification_flag': ::__s__::%G?::__e__::,%n"
                             + "  'signer': ::__s__::%GS::__e__::,%n"
                             + "  'signer_key': ::__s__::%GK::__e__::,%n"
                             + "  'author': {%n"
                             + "    'name': ::__s__::%aN::__e__::,%n"
                             + "    'email': ::__s__::%aE::__e__::,%n"
                             + "    'date': ::__s__::%ad::__e__::%n"
                             + "  },%n"
                             + "  'commiter': {%n"
                             + "    'name': ::__s__::%cN::__e__::,%n"
                             + "    'email': ::__s__::%cE::__e__::,%n"
                             + "    'date': ::__s__::%cd::__e__::%n"
                             + "  }%n},");

                    await (await this.processBuilderFactory.Git())
                          .WithWorkingDirectory(checkoutLocation)
                          .WithArguments(args.ToArray())
                          .WithOutputWriter(sbi)
                          .WithErrorWriter(sbErrori)
                          .Execute();

                    await sbi.FlushAsync();
                    await sbErrori.FlushAsync();

                    var contentLog = sbi.GetStringBuilder().ToString();

                    var sPrefix = "::__s__::";
                    var ePrefix = "::__e__::";

                    while (contentLog.Contains(sPrefix))
                    {
                        var si = contentLog.IndexOf(sPrefix, StringComparison.OrdinalIgnoreCase);

                        var se = contentLog.IndexOf(ePrefix, si, StringComparison.OrdinalIgnoreCase);

                        var start = contentLog.Substring(0, si);
                        var middle = contentLog.Substring(si + sPrefix.Length, se - (si + sPrefix.Length));
                        var end = contentLog.Substring(se + ePrefix.Length);
                        contentLog = string.Concat(start, '\'', middle.Replace("'", "\\'"), '\'', end);
                    }

                    contentLog = $"[{contentLog.Trim().TrimEnd(new char[] {','})}]";

                    return this.ser.Deserialize<List<GitLog>>(contentLog);
                }
            }
        }

        private async Task<List<FileChange>> GetFileChanges(IsolatedBranchData line, string checkoutLocation)
        {
            using (var sbi = new StringWriter())
            {
                using (var sbErrori = new StringWriter())
                {
                    var parameters = new List<string>();
                    parameters.Add("log");
                    if (!string.IsNullOrWhiteSpace(line.Hash))
                    {
                        parameters.Add(line.Hash);
                    }

                    parameters.Add("--name-status");
                    parameters.Add("--compact-summary");
                    parameters.Add("--date=format:%Y-%m-%d %H:%M:%S");
                    parameters.Add("--max-count=" + MaxCount);
                    parameters.Add("--no-renames");
                    parameters.Add("--pretty=format:::__s__::%H");
                    await (await this.processBuilderFactory.Git())
                          .WithWorkingDirectory(checkoutLocation)
                          .WithArguments(parameters.ToArray())
                          .WithOutputWriter(sbi)
                          .WithErrorWriter(sbErrori)
                          .Execute();

                    await sbi.FlushAsync();
                    await sbErrori.FlushAsync();

                    var contentLog = sbi.GetStringBuilder().ToString();

                    var sPrefix = "::__s__::";

                    List<FileChange> changes = new List<FileChange>();
                    using (var sr = new StringReader(contentLog))
                    {
                        string lineContent = null;
                        string commitHash = null;
                        while ((lineContent = sr.ReadLine()) != null)
                        {
                            var lineClean = lineContent.Trim();
                            if (lineClean.StartsWith(sPrefix))
                            {
                                commitHash = lineClean.Substring(sPrefix.Length).Trim();
                            }
                            else if (string.IsNullOrWhiteSpace(lineClean))
                            {
                            }
                            else if (lineClean.StartsWith("M"))
                            {
                                changes.Add(new FileChange
                                            {
                                                Commit = commitHash,
                                                File = lineClean.Substring(1).Trim(),
                                                ChangeType = FileChangeType.Modified,
                                            });
                            }
                            else if (lineClean.StartsWith("D"))
                            {
                                changes.Add(new FileChange
                                            {
                                                Commit = commitHash,
                                                File = lineClean.Substring(1).Trim(),
                                                ChangeType = FileChangeType.Deleted,
                                            });
                            }
                            else if (lineClean.StartsWith("A"))
                            {
                                changes.Add(new FileChange
                                            {
                                                Commit = commitHash,
                                                File = lineClean.Substring(1).Trim(),
                                                ChangeType = FileChangeType.Added,
                                            });
                            }
                        }
                    }

                    return changes;
                }
            }
        }

//        private async Task GetLatest()
//        {
////            var state = await runner.GetOrigin();
//            while (!state.In(IsolatedCheckoutState.CompletedFail, IsolatedCheckoutState.CompletedSuccess))
//            {
//                await Task.Delay(TimeSpan.FromSeconds(3));
//                state = await runner.GetOrigin();
//            }
//
//            await runner.Pull();
//        }

        private async Task PrepProfile()
        {
            var pathOrigins = this.directoryProvider.OriginsRoot();

            var profile = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

            var sshFolder = Directory.CreateDirectory(Path.Combine(profile, ".ssh"));
            var pubs = sshFolder.GetFiles("*.pub");

            foreach (var pub in pubs)
            {
                var identityFile = Path.GetFileNameWithoutExtension(pub.FullName);
                Console.WriteLine(identityFile);
                await (await this.processBuilderFactory.CreateAsUser())
                      .WithExecutable("ssh")
                      .WithWorkingDirectory(pathOrigins)
                      .WithArguments(new[] {"-i", identityFile})
                      .WithOutputWriter(Console.Out)
                      .WithErrorWriter(Console.Out)
                      .Execute();
            }
        }

        private async Task CloneAsync(string cloneLocation, string destinationPath, string branch)
        {
            if (Directory.Exists(Path.Combine(destinationPath, ".git")))
            {
                await (await this.processBuilderFactory.Git())
                      .WithWorkingDirectory(destinationPath)
                      .WithArguments(new[] {"pull", "origin", branch})
                      .WithOutputWriter(Console.Out)
                      .WithErrorWriter(Console.Out)
                      .Execute();
            }
            else
            {
                await (await this.processBuilderFactory.Git())
                      .WithWorkingDirectory(Path.GetDirectoryName(destinationPath))
                      .WithArguments(new[] {"clone", cloneLocation, Path.GetFileName(destinationPath), "--branch", branch})
                      .WithOutputWriter(Console.Out)
                      .WithErrorWriter(Console.Out)
                      .Execute();
            }
        }

        public async Task<IsolatedBranchLogData> GetLogs(GitRepositoryBranchData data)
        {
//            var repoData = new GitRepositoryData
//                           {
//                               CloneLocation = data.CloneLocation
//                           };
//            var runner = this.Runner();

            var checkoutLocation = this.directoryProvider.GetCheckoutLocation(data);
            await this.CloneAsync(data.Repository.CloneLocation, checkoutLocation, data.Branch.Name);
//            await this.GetLatest(runner);

            var comp = StringComparison.InvariantCultureIgnoreCase;

            IsolatedBranchLogData returnValue = new IsolatedBranchLogData {Name = data.Branch.Name, Hash = data.Branch.Hash};

            var line = data.Branch;
            var items = await this.GetLogItems(line, checkoutLocation);
            var changes = await this.GetFileChanges(line, checkoutLocation);

            var invariantCulture = CultureInfo.InvariantCulture;
            returnValue.Logs = items.Select(x => new IsolatedCheckoutLogData
                                                 {
                                                     Id = x.commit,
                                                     Author = x.author.name,
                                                     Message = x.subject,
                                                     When = DateTime.ParseExact(x.author.date, "yyyy-MM-dd HH:mm:ss", invariantCulture),
                                                     FilesAdded = changes.Where(y => y.ChangeType == FileChangeType.Added)
                                                                         .Where(y => string.Equals(y.Commit, x.commit, comp))
                                                                         .Select(y => y.File)
                                                                         .ToArray(),
                                                     FilesRemoved = changes.Where(y => y.ChangeType == FileChangeType.Deleted)
                                                                           .Where(y => string.Equals(y.Commit, x.commit, comp))
                                                                           .Select(y => y.File)
                                                                           .ToArray(),
                                                     FilesChanged = changes.Where(y => y.ChangeType == FileChangeType.Modified)
                                                                           .Where(y => string.Equals(y.Commit, x.commit, comp))
                                                                           .Select(y => y.File)
                                                                           .ToArray(),
                                                 })
                                    .ToArray();

            return returnValue;
        }
    }
}