namespace Base2art.CiCd.SourceControl.Web.Git
{
    using System;
    using System.Security.Principal;

    public static class Principals
    {
        public static void RequireSignedIn(this IPrincipal principal)
        {
            var isAuthenticated = principal?.Identity?.IsAuthenticated;

            if (!isAuthenticated.GetValueOrDefault())
            {
                throw new NotAuthenticatedException();
            }
        }

        public static void RequireSignedIn(this IRequireAuthProvider auth, IPrincipal principal)
        {
            if (auth.Value)
            {
                principal.RequireSignedIn();
            }
        }
    }

    public class NotAuthenticatedException : Exception
    {
    }
}