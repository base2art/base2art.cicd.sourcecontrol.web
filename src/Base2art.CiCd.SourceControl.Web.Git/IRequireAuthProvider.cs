namespace Base2art.CiCd.SourceControl.Web.Git
{
    public interface IRequireAuthProvider
    {
        bool Value { get; }
    }
}