﻿namespace Base2art.CiCd.SourceControl.Web.Git
{
    using System;
    using System.IO;
    using System.Linq;
    using Hashing;
    using Public.Resources;

    public static class Enumerables
    {
        public static bool In<T>(this T item, params T[] items)
            where T : struct, IComparable
        {
            return (items ?? new T[0]).Any(y => item.CompareTo(y) == 0);
        }

        public static string GetCheckoutLocation(this IDirectoryProvider basePath, GitRepositoryBranchData data)
        {
            return Path.Combine(basePath.OriginsRoot().FullName, data.CloneId().ToString("N"));
        }

        public static Guid CloneId(this GitRepositoryBranchData data)
        {
            var cloneLocation = data.Repository.CloneLocation;
            return (cloneLocation.HashAsGuid() + ":" + (data.Branch.Name ?? string.Empty).HashAsGuid()).HashAsGuid();
        }
    }
}