namespace Base2art.CiCd.SourceControl.Web.Git
{
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Threading.Tasks;

    public static class Zipper
    {
        public static async Task<byte[]> ZipToBytes(this DirectoryInfo destinationPath)
        {
            using (var zipToOpen = new MemoryStream())
            {
                using (var archive = new ZipArchive(zipToOpen, ZipArchiveMode.Create, true))
                {
                    DoZipRecursive(archive, destinationPath, new string[0]);
                }

                await zipToOpen.FlushAsync();
                zipToOpen.Seek(0L, SeekOrigin.Begin);
                return zipToOpen.ToArray();
            }
        }

        private static void DoZipRecursive(ZipArchive archive, DirectoryInfo folder, string[] bases)
        {
            foreach (var file in folder.EnumerateFiles())
            {
                archive.CreateEntryFromFile(file.FullName, string.Join("/", bases.Concat(new[] {file.Name})), CompressionLevel.Optimal);
            }

            foreach (var dir in folder.EnumerateDirectories().Where(dir => dir.Name != ".git"))
            {
                DoZipRecursive(archive, dir, bases.Concat(new[] {dir.Name}).ToArray());
            }
        }
    }
}