﻿/*
namespace Base2art.CiCd.SourceControl.Web.Git
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Base2art.Diagnostic;
    using Data;
    using Hashing;
    using Public.Resources;
    using Resources;

    public class KeyedOriginRunner : IKeyedOriginRunner
    {
        private readonly DirectoryProvider info;
        private readonly IProcessBuilderFactory processBuilderFactory;
        private readonly Guid processId;
        private readonly GitRepositoryData projectSourceControlData;
        private readonly ISourceControlStorage stateLookup;

        public KeyedOriginRunner(IProcessBuilderFactory processBuilderFactory,
                                 DirectoryProvider info,
                                 GitRepositoryData projectSourceControlData,
                                 ISourceControlStorage stateLookup,
                                 Guid processId)
        {
            this.processBuilderFactory = processBuilderFactory;
            this.info = info;
            this.projectSourceControlData = projectSourceControlData;
            this.stateLookup = stateLookup;
            this.processId = processId;
        }

        public async Task<IsolatedCheckoutState> GetOrigin()
        {
            var cloneId = await this.Setup();

            var status = await this.stateLookup.GetStatus(cloneId, this.processId);
            if (status != IsolatedCheckoutState.Pending)
            {
                return status;
            }

            await this.stateLookup.SetStatus(cloneId, this.processId, IsolatedCheckoutState.Working);

            var pathOrigins = this.info.OriginsRoot();

            var profile = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

            var sshFolder = Directory.CreateDirectory(Path.Combine(profile, ".ssh"));
            var pubs = sshFolder.GetFiles("*.pub");

            foreach (var pub in pubs)
            {
                var identityFile = Path.GetFileNameWithoutExtension(pub.FullName);
                Console.WriteLine(identityFile);
                await (await this.processBuilderFactory.CreateAsUser())
                      .WithExecutable("ssh")
                      .WithWorkingDirectory(pathOrigins)
                      .WithArguments(new[] {"-i", identityFile})
                      .WithOutputWriter(Console.Out)
                      .WithErrorWriter(Console.Out)
                      .Execute();
            }

            await (await this.processBuilderFactory.Git())
                  .WithWorkingDirectory(pathOrigins)
                  .WithArguments($"clone \"{this.projectSourceControlData.CloneLocation.Trim()}\" {cloneId:N}")
                  .WithOutputWriter(Console.Out)
                  .WithErrorWriter(Console.Out)
                  .Execute();
            await this.stateLookup.SetStatus(cloneId, this.processId, IsolatedCheckoutState.CompletedSuccess);
            return IsolatedCheckoutState.CompletedSuccess;
        }

        public async Task<IsolatedCheckoutState> Pull()
        {
            var cloneId = await this.Setup();

            var status = await this.stateLookup.GetStatus(cloneId, this.processId);
            if (status != IsolatedCheckoutState.CompletedSuccess)
            {
                return status;
            }

            await this.stateLookup.SetStatus(cloneId, this.processId, IsolatedCheckoutState.Working);
            var pathOrigins = this.GetCheckoutLocation();

//            await this.processBuilderFactory
//                      .Create()
//                      .WithExecutable(@"C:\WINDOWS\system32\whoami.exe")
//                      .WithLoadedUserProfile()
//                      .WithWorkingDirectory(pathOrigins)
//                      .WithOutputWriter(Console.Out)
////                      .WithErrorWriter(writer)
//                      .Execute();

            Directory.CreateDirectory(pathOrigins);
            await (await this.processBuilderFactory.Git())
                  .WithWorkingDirectory(pathOrigins)
//                      .WithOutputWriter(writer)
//                      .WithErrorWriter(writer)
                  .WithArguments("fetch --prune")
                  .Execute();

            await (await this.processBuilderFactory.Git())
                  .WithWorkingDirectory(pathOrigins)
//                      .WithOutputWriter(writer)
//                      .WithErrorWriter(writer)
                  .WithArguments("pull")
                  .Execute();

            await this.stateLookup.SetStatus(cloneId, this.processId, IsolatedCheckoutState.CompletedSuccess);
            return IsolatedCheckoutState.CompletedSuccess;
        }

        public string GetCheckoutLocation() => Path.Combine(this.info.OriginsRoot().FullName, this.GetCloneId().ToString("N"));

        public Guid GetCloneId()
        {
            
            return this.projectSourceControlData.CloneId();
        }

        private async Task<Guid> Setup()
        {
            var cloneId = this.GetCloneId();

            var status = await this.stateLookup.GetStatus(cloneId, this.processId);

            if (status == IsolatedCheckoutState.Unknown)
            {
                await this.stateLookup.SetStatus(cloneId, this.processId, IsolatedCheckoutState.Pending);
            }

            return cloneId;
        }
    }
}
*/