namespace Base2art.CiCd.SourceControl.Web.Git.Hashing
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    internal static class Hasher
    {
        public static Guid HashAsGuid(this string value)
        {
            return Create(value, x => new Md5HashResult(x), MD5.Create).AsGuid();
        }

        private static TResult Create<T, TResult>(string value, Func<byte[], TResult> creator, Func<T> provider)
            where T : HashAlgorithm
            where TResult : HashResult
        {
            var inputBytes = DefaultEncoding().GetBytes(value ?? string.Empty);
            using (var hashAlgorithm = provider())
            {
                return creator(hashAlgorithm.ComputeHash(inputBytes));
            }
        }

        private static Encoding DefaultEncoding() => Encoding.UTF8;
    }
}