namespace Base2art.CiCd.SourceControl.Web.Git
{
    using System.IO;
    using Public.Resources;

    public interface IDirectoryProvider
    {
        DirectoryInfo GetCheckoutRoot();

        string GetWorkingDir();

        DirectoryInfo OriginsRoot();

        DirectoryInfo DataDirectory();
    }
}